const UserModel = require("../models/user_model");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const { response } = require("express");
const CrewModel = require("../models/crew_model");
require("dotenv").config();

module.exports = {
    // Login
    signIn(req, res = response) {
        let { email, password } = req.body;

        // Buscar usuario
        UserModel.findOne({
                where: {
                    "email-user": email,
                },
            })
            .then((user) => {
                if (!user) {
                    return res.status(404).json({
                        ok: false,
                        body: "Usuario con este correo no encontrado",
                    });
                } else {
                    if (bcrypt.compareSync(password, user.dataValues["password-user"])) {
                        // Creamos el token

                        let token = jwt.sign({ user: user }, process.env.AUTH_SECRET, {
                            expiresIn: process.env.AUTH_EXPIRES,
                        });
                        return res.json({
                            ok: true,
                            user: user,
                            token: token,
                        });
                    } else {
                        // Unauthorized Access
                        return res.status(401).json({
                            ok: false,
                            body: "Contraseña incorrecta",
                        });
                    }
                }
            })
            .catch((err) => {
                res.status(500).json({
                    ok:false,
                    error:JSON.stringify( err.Error)
                });
            });
    },

    // Registro
    signUp(req, res = response) {
        // Encriptamos la contraseña
        let password = bcrypt.hashSync(
            req.body["password-user"],
            Number(process.env.AUTH_ROUNDS)
        );
        // Crear un usuario

        UserModel.create({
                "email-user": req.body["email-user"],
                "password-user": password,
                "name-user": req.body["name-user"],
                "id-rol": req.body["id-rol"],
            })
            .then((user) => {
                // Creamos el token
                let token = jwt.sign({ user: user }, process.env.AUTH_SECRET, {
                    expiresIn: process.env.AUTH_EXPIRES,
                });

                res.json({
                    ok: true,
                    user: user,
                    token: token,
                });
            })
            .catch((err) => {
                res.status(500).json(err);
            });
    },

    verifyToken(req, res = response){
        let password = req.body["password-user"];
          

        UserModel.findOne({
            where: {
                "email-user": req.email_user,
            },
        })
        .then((user) => {
            if (!user) {
                return res.status(404).json({
                    ok: false,
                    body: "Usuario con este correo no encontrado",
                });
            } else {
      
                    return res.json({
                        ok: true,
                        user: user,
                    });
                
            }
        })
        .catch((err) => {
            return res.status(500).json({
                ok: false,
                body: `${err}`,
            });
        });
    }
};
