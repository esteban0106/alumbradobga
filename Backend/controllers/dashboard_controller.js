const querys = require("../helpers/querys");
const moment = require("moment");
const { query } = require("../database/conectar_db");
const Excel = require("exceljs");
const mailer = require("../templates/email");

const pendingXcrew = async(req, res) => {
    try {
        let crew = req.body.crew;
        const infoFaultXcrew = await querys.faultPending(crew);
        let o = [];
        let crewPending = [];
        infoFaultXcrew.forEach((element) => o.push(element["id-crew"]));
        let ids = new Set(o);
        ids.forEach((idCrew) => {
            let elementFiltersByIdCrew = infoFaultXcrew.filter(
                (element) => element["id-crew"] === idCrew
            );

            let output = {
                Crew: idCrew,
                Pending: elementFiltersByIdCrew.map((p) => {
                    return {
                        typeFault: p["name-type-output"],
                        NameDevice: p["name-device"],
                        idAddress: p["name-address"],
                        ifFailure: p["id-failure"],
                        TimeDetect: p["fail-detect-date"],
                    };
                }),
            };
            crewPending = [...crewPending, output];
        });
        res.json({
            ok: true,
            body: crewPending,
        });
    } catch (error) {
        res.status(500).json({
            ok: false,
            error,
        });
    }
};
const crewXcommune = async(req, res) => {
    try {
        let communes = req.body.commune;
        const infoCrewEmploy = await querys.crewInfoEmployees(communes);

        let o = [];
        let crewEmploy = [];
        infoCrewEmploy.forEach((element) => o.push(element["name-crew"]));
        let ids = new Set(o);
        ids.forEach((nameCrew) => {
            let elementFiltersByNameCrew = infoCrewEmploy.filter(
                (element) => element["name-crew"] === nameCrew
            );

            if (elementFiltersByNameCrew[0]["name-employee"]) {
                let output = {
                    Crew: nameCrew,
                    idCrew: elementFiltersByNameCrew[0]["id-crew"],
                    Commune: elementFiltersByNameCrew[0]["name-commune"],
                    IdCommune: elementFiltersByNameCrew[0]["id-commune"],
                    employee: elementFiltersByNameCrew.map((p) => {
                        return {
                            id: p["id-employee"],
                            name: p["name-employee"],
                            lastname: p["lastname-employee"],
                            phone: p["phone-employee"],
                            rol: p["name-rol"],
                            // idCrew: p["id-crew"],
                        };
                    }),
                };
                crewEmploy = [...crewEmploy, output];
            }
            if (elementFiltersByNameCrew[0]["name-employee"] == null) {
                let output = {
                    Crew: nameCrew,
                    idCrew: elementFiltersByNameCrew[0]["id-crew"],
                    Commune: elementFiltersByNameCrew[0]["name-commune"],
                    IdCommune: elementFiltersByNameCrew[0]["id-commune"],
                    employee: null,
                };
                crewEmploy = [...crewEmploy, output];
            }
        });

        res.json({
            ok: true,
            body: crewEmploy,
        });
    } catch (error) {
        res.status(500).json({
            ok: false,
            error,
        });
    }
};
const typeFault = async(req, res) => {
    try {
        let initDate = req.body.initDate;
        let endDate = req.body.endDate;
        const typeFault = await querys.infoTypeFault(initDate, endDate);

        res.json({
            ok: true,
            body: typeFault,
        });
    } catch (error) {
        res.status(500).json({
            ok: false,
            error,
        });
    }
};

const infoLast24Hours = async(req, res) => {
    try {
        let idDevice = req.body.idDevice;
        // console.log(moment().format("YYYY-MM-DDHH:mm:ss"));
        let nowDate = moment().subtract(1, "days").format("YYYY-MM-DDTHH:mm:ss");

        let info = await querys.info24Ago(nowDate, idDevice);

        if (info.length === 0) {
            info = 0;
        }

        return res.json({
            ok: true,
            body: info,
        });
    } catch (error) {
        res.status(500).json({
            ok: false,
            error,
        });
    }
};
const crewCommune = async(req, res) => {
    if (req.id_rol === 3)
        return res
            .status(401)
            .json({ ok: false, body: "Don't have authentication" });
    let communes = req.body.communes;

    try {
        const getCrewCommune = await querys.getCrewCommune(communes);
        res.json({
            ok: true,
            body: getCrewCommune,
        });
    } catch {
        res.status(500).json({
            ok: false,
            error,
        });
    }
};
const FaultFix = async(req, res) => {
    if (req.id_rol === 3)
        return res
            .status(401)
            .json({ ok: false, body: "Don't have authentication" });
    try {
        let initDate = req.body.initDate;
        let endDate = req.body.endDate;
        let data = await querys.getDateFaultFix(initDate, endDate);
        let IdData = data.map((dates) => {
            return {
                data: moment.duration(
                    moment(dates["fix-date"] ? dates["fix-date"] : Date.now()).diff(
                        dates["fail-detect-date"]
                    )
                ),
                idCrew: dates["id-crew"],
            };
        });
        let IdData2 = IdData.map((dates2) => {
            return {
                duration: Number([
                    parseInt(dates2.data.asHours().toFixed(0)) * 3600 +
                    parseInt(dates2.data.minutes()) * 60 +
                    parseInt(dates2.data.seconds()),
                ]),
                idcrew: Number(dates2["idCrew"]),
            };
        });
        let o = [];
        let crewPerformance = [];
        IdData2.forEach((element) => o.push(element.idcrew));

        let ids = new Set(o);

        ids.forEach((idCrew) => {
            let elementFiltersById = IdData2.filter(
                (element) => element.idcrew === idCrew
            );
            let longitud = elementFiltersById.length;
            let sumByID = elementFiltersById.reduce((acumulador, nextValor) => {
                acumulador.duration = acumulador.duration + nextValor.duration;
                return acumulador;
            });
            let output = {
                idCrew,
                performance: sumByID.duration / longitud,
            };
            crewPerformance = [...crewPerformance, output];
        });
        crewPerformance = crewPerformance.sort((a, b) => {
            if (a.idCrew < b.idCrew) return -1;
            if (a.idCrew == b.idCrew) return 1;
            if (a.idCrew > b.idCrew) return 1;
        });
        res.json({
            ok: true,
            body: crewPerformance,
        });
    } catch (error) {
        res.status(500).json({
            ok: false,
            error,
        });
    }
};
const topDeviceFault = async(req, res) => {
    let limite = req.body.limite;
    let initDate = req.body.initDate;
    let endDate = req.body.endDate;
    if (req.id_rol === 3)
        return res
            .status(401)
            .json({ ok: false, body: "Don't have authentication" });
    try {
        let topDevice = await querys.topFault(limite, initDate, endDate);
        res.json({
            ok: true,
            body: topDevice,
        });
    } catch (error) {
        res.status(500).json({
            ok: false,
            error,
        });
    }
};

const responseTime = async(req, res) => {
    let initDate = req.body.initDate;
    let endDate = req.body.endDate;
    if (req.id_rol === 3)
        return res
            .status(401)
            .json({ ok: false, body: "Don't have authentication" });
    try {
        let responseTime = await querys.ResponseTimeRange( initDate, endDate);
        res.json({
            ok: true,
            body: responseTime,
        });
    } catch (error) {
        res.status(500).json({
            ok: false,
            error,
        });
    }
};

const faultCommune = async(req, res) => {
    if (req.id_rol === 3)
        return res
            .status(401)
            .json({ ok: false, body: "Don't have authentication" });
    try {
        let initDate = req.body.initDate;
        let endDate = req.body.endDate;
        let getFaults = await querys.getFault(initDate, endDate);
        res.json({
            ok: true,
            body: getFaults,
        });
    } catch (error) {
        res.status(500).json({
            ok: false,
            error,
        });
    }
};

/*
 
 ########  ######## ########   #######  ########  ########    ########    ###    ##     ## ##       ######## 
 ##     ## ##       ##     ## ##     ## ##     ##    ##       ##         ## ##   ##     ## ##          ##    
 ##     ## ##       ##     ## ##     ## ##     ##    ##       ##        ##   ##  ##     ## ##          ##    
 ########  ######   ########  ##     ## ########     ##       ######   ##     ## ##     ## ##          ##    
 ##   ##   ##       ##        ##     ## ##   ##      ##       ##       ######### ##     ## ##          ##    
 ##    ##  ##       ##        ##     ## ##    ##     ##       ##       ##     ## ##     ## ##          ##    
 ##     ## ######## ##         #######  ##     ##    ##       ##       ##     ##  #######  ########    ##    
 
*/
const reportFault = async(req, res) => {
    if (req.id_rol === 3)
        return res
            .status(401)
            .json({ ok: false, body: "Don't have authentication" });
    try {
        let initDate = req.body.initDate;
        let endDate = req.body.endDate;
        let getInfo = await querys.reportFails(initDate, endDate);
        let getInfo2 = await querys.reportOutputs(initDate, endDate);

        const workbookResp = new Excel.Workbook(); //Se crea el libro de Excel
        var worksheetOut = workbookResp.addWorksheet("Outputs"); //Se crea la de excel
        var worksheetFails = workbookResp.addWorksheet("Failures"); //Se crea la hoja de excel

        //  Encabezados de la hoja Excel

        worksheetOut.columns = [{
                header: "serial",
                key: "id",
                width: 18,
                style: { alignment: { horizontal: "center" } },
            },
            {
                header: "Codigo dispositivo",
                key: "idDevice",
                width: 18,
                style: { alignment: { horizontal: "center" } },
            },
            {
                header: "Nombre dispositivo",
                key: "nameDevice",
                width: 18,
                style: { alignment: { horizontal: "center" } },
            },
            {
                header: "Codigo comuna",
                key: "idCommune",
                width: 18,
                style: { alignment: { horizontal: "center" } },
            },
            {
                header: "Comuna",
                key: "nameCommune",
                width: 18,
                style: { alignment: { horizontal: "center" } },
            },
            {
                header: "Fecha",
                key: "DATE",
                width: 18,
                style: { alignment: { horizontal: "center" } },
            },
            {
                header: "Activo",
                key: "working",
                width: 18,
                style: { alignment: { horizontal: "center" } },
            },
            {
                header: "Latitud",
                key: "latDevice",
                width: 18,
                style: { alignment: { horizontal: "center" } },
            },
            {
                header: "Longitud",
                key: "lonDevice",
                width: 18,
                style: { alignment: { horizontal: "center" } },
            },
            {
                header: "Corriente mA",
                key: "current",
                width: 18,
                style: { alignment: { horizontal: "center" } },
            },
            {
                header: "Voltaje mV",
                key: "voltage",
                width: 18,
                style: { alignment: { horizontal: "center" } },
            },
            {
                header: "Potencia mW",
                key: "POWER",
                width: 18,
                style: { alignment: { horizontal: "center" } },
            },
            {
                header: "Codigo tipo registro",
                key: "typeId",
                width: 18,
                style: { alignment: { horizontal: "center" } },
            },
            {
                header: "tipo de registro",
                key: "typeName",
                width: 18,
                style: { alignment: { horizontal: "center" } },
            },
        ];

        worksheetOut.getRow(1).font = { bold: true };

        // Se agregan las filas del Array
        worksheetOut.addRows(getInfo2);
        console.log(getInfo);

        worksheetFails.columns = [{
                header: "Serial",
                key: "id",
                width: 18,
                style: { alignment: { horizontal: "center" } },
            },
            {
                header: "Codigo dispositivo",
                key: "idDevice",
                width: 18,
                style: { alignment: { horizontal: "center" } },
            },
            {
                header: "Nombre dispositivo",
                key: "nameDevice",
                width: 18,
                style: { alignment: { horizontal: "center" } },
            },
            {
                header: "Codigo comuna",
                key: "idCommune",
                width: 18,
                style: { alignment: { horizontal: "center" } },
            },
            {
                header: "Nombre comuna",
                key: "nameCommune",
                width: 18,
                style: { alignment: { horizontal: "center" } },
            },
            {
                header: "Latitud",
                key: "latDevice",
                width: 18,
                style: { alignment: { horizontal: "center" } },
            },
            {
                header: "Longitud",
                key: "lonDevice",
                width: 18,
                style: { alignment: { horizontal: "center" } },
            },
            {
                header: "Activo",
                key: "working",
                width: 18,
                style: { alignment: { horizontal: "center" } },
            },
            {
                header: "Codigo status",
                key: "statusId",
                width: 18,
                style: { alignment: { horizontal: "center" } },
            },
            {
                header: "status",
                key: "statusName",
                width: 18,
                style: { alignment: { horizontal: "center" } },
            },
            {
                header: "Id empleado",
                key: "employeeId",
                width: 18,
                style: { alignment: { horizontal: "center" } },
            },
            {
                header: "Empleado a cargo",
                key: "employeeName",
                width: 18,
                style: { alignment: { horizontal: "center" } },
            },
            {
                header: "Codigo cuadrilla",
                key: "crewId",
                width: 18,
                style: { alignment: { horizontal: "center" } },
            },
            {
                header: "Cuadrilla",
                key: "crewName",
                width: 18,
                style: { alignment: { horizontal: "center" } },
            },
            {
                header: "Fecha reporte",
                key: "detectDate",
                width: 18,
                style: { alignment: { horizontal: "center" } },
            },
            {
                header: "Fecha de arreglo",
                key: "fixDate",
                width: 18,
                style: { alignment: { horizontal: "center" } },
            },
            {
                header: "Codigo falla",
                key: "faultId",
                width: 18,
                style: { alignment: { horizontal: "center" } },
            },
            {
                header: "falla",
                key: "faultName",
                width: 18,
                style: { alignment: { horizontal: "center" } },
            },
        ];

        worksheetFails.getRow(1).font = { bold: true };

        // Se agregan las filas del Array
        worksheetFails.addRows(getInfo);
        console.log(getInfo2);

        worksheetFails.getRow(1).font = { bold: true };

        // Se agregan las filas del Array
        worksheetFails.addRows(getInfo);
        console.log(getInfo2);

        try {
            res.setHeader(
                "Content-Type",
                "application/vndopenxmlformats-officedocument.spreadsheetml.sheet"
            );

            res.setHeader(
                "Content-Disposition",
                `attachment; filename=reporte-${initDate + endDate}-${Date.now()}.xlsx`
            );

            await workbookResp.xlsx.write(res); //Se escribe el archivo de Excel

            res.status(200).end();
        } catch (error) {
            return res.status(200).json({
                status: 500,
                message: error.message,
                body: null,
            });
        }
    } catch (error) {
        res.status(500).json({
            ok: false,
            error,
        });
    }
};

const getInfoQueryDate = async(req, res) => {
    if (req.id_rol === 3)
        return res
            .status(401)
            .json({ ok: false, body: "Don't have authentication" });
    try {
        let initDate = req.body.initDate;
        let endDate = req.body.endDate;
        let getInfo = await querys.getInfoGeneral(initDate, endDate);
        res.json({
            ok: true,
            body: getInfo,
        });
    } catch (error) {
        res.status(500).json({
            ok: false,
            error,
        });
    }
};

const devicesXcommune = async(req, res) => {
    if ([3,4].includes(req.id_rol) )
        return res
            .status(401)
            .json({ ok: false, body: "Don't have authentication" });
    try {
        let initDate = req.body.initDate;
        let endDate = req.body.endDate;
        let getInfo = await querys.devicesXcommune(initDate, endDate);
        res.json({
            ok: true,
            body: getInfo,
        });
    } catch (error) {
        res.status(500).json({
            ok: false,
            error,
        });
    }
};

const outputxtype = async(req, res) => {
    if ([3,4].includes(req.id_rol) )
        return res
            .status(401)
            .json({ ok: false, body: "Don't have authentication" });
    try {
        let initDate = req.body.initDate;
        let endDate = req.body.endDate;
        let getInfo = await querys.outputxtype(initDate, endDate);
        res.json({
            ok: true,
            body: getInfo,
        });
    } catch (error) {
        res.status(500).json({
            ok: false,
            error,
        });
    }
};



module.exports = {
    faultCommune,
    getInfoQueryDate,
    topDeviceFault,
    FaultFix,
    crewCommune,
    infoLast24Hours,
    typeFault,
    crewXcommune,
    pendingXcrew,
    reportFault,
    responseTime,
    devicesXcommune,
    outputxtype
};