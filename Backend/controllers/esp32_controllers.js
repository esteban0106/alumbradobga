const OutputModel = require("../models/output_model");
const FailureModel = require("../models/failure_model");
const querys = require("../helpers/querys");
const mailer = require("../templates/email");
const moment = require("moment");

const process_msg = async(topic, message) => {
    try {
        if (topic == "values") {
            let msg = message.toString();
            let sp = msg.split(",");
            let shunt = sp[0];
            let current = sp[1];
            let power = sp[2];
            let voltage = sp[3];
            let idDevice = sp[4];
            let typeFault = 1;

            let dataWorkDevice = await querys.typeFault(idDevice);
            let dateNow = moment().format("HH:mm");

            if (
                (current > dataWorkDevice[0]["normal-current"] + 400 &&
                    dateNow > "23:00") ||
                dateNow < "11:00"
            )
                typeFault = 2;
            if (
                (current < dataWorkDevice[0]["normal-current"] - 60 &&
                    dateNow > "23:00") ||
                dateNow < "11:00"
            )
                typeFault = 3;
            if (current > 1 && dateNow > "11:00" && dateNow < "23:00") typeFault = 4;
            if ((current < 1 && dateNow > "23:00") || dateNow < "11:00")
                typeFault = 5;

            if (typeFault != 1) {
                let info = await querys.infoEmail(idDevice);
                let work = await querys.working(idDevice);
                let IdCrew = await querys.crew(idDevice);
                let employees = await querys.email(IdCrew[0]["crew"]);
                let IdEmployees = employees.map((employee) => {
                    return employee["id-user"];
                });

                if (work[0]["working"] === 1) {
                    FailureModel.create({
                        "status-failure": 1,
                        "id-device": idDevice,
                        "id-crew": IdCrew[0]["crew"],
                        "type-fault": typeFault,
                    });
                    mailer.enviar_mail(IdEmployees, info);
                    await querys.workUpdateOff(idDevice);
                }
            }

            OutputModel.create({
                "id-device": idDevice,
                "current-output": current,
                "shunt-voltage-output": shunt,
                "power-output": power,
                "type-output": typeFault,
                "fecha-output": moment().format("YYYY-MM-DDTHH:mm:ss"),
            });
            console.log(
                `shunt: ${shunt}, current: ${current}, Power: ${power}, Voltage: ${voltage}, Device: ${idDevice}`
            );
        }
    } catch (e) {
        console.log(e);
    }
};

module.exports = {
    process_msg,
};