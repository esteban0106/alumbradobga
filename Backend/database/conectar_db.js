const { Sequelize } = require('sequelize');
const mysql = require('mysql2');
require('dotenv').config();

const conn = new Sequelize(
    process.env.dbName,
    process.env.dbUser,
    process.env.dbPassword, {
    dialect: 'mysql',
    host: process.env.dbHost,
    port: process.env.dbPort,
    logging: false, // para no mostrar por consola las sentencias SQL ejecutadas.

    dialectOptions: {

        connectTimeout: 60000

    },
    pool: {
        maxIdleTime: 30,
        idle: 20000,
        evict: 15000,
        acquire: 30000,
        max: 15,
        min: 0,
    }
});


module.exports = conn;