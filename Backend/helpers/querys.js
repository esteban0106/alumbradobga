const { QueryTypes } = require("sequelize");
const conn = require("../database/conectar_db");
const FailureModel = require("../models/failure_model");
const CommuneModel = require("../models/commune_model");
const CrewModel = require("../models/crew_model");
const EmployeeModel = require("../models/employee_model");
const DeviceModel = require("../models/device_model");
const { response } = require("express");
const UserModel = require("../models/user_model");

let querys = {};

//------------SELECT SEQUELIZE-----------------

querys.email = async(crewE) => {
    const employ = await EmployeeModel.findAll({
        where: {
            "id-crew": crewE,
        },
    });

    return employ;
};

querys.user = async(email) => {
    const user = await UserModel.findAll({
        where: {
            "email-user": email,
        },
    });

    return user;
};

querys.crew = async(device) => {
    const crew = await DeviceModel.findAll({
        attributes: ["crew"],
        where: {
            "id-device": device,
        },
    });

    return crew;
};

//------------SELECT SQL---------------------

querys.infoEmail = async(idDevice) => {
    try {
        const infoEmailS = await conn.query(
            "SELECT c.`id-crew` ,d.`id-device`,d.`name-device`,d.`lon-device`,d.`lat-device`,c.`name-crew`,co.`name-commune`,a.`name-address` from device d LEFT JOIN crew c ON c.`id-crew`=d.`id-device` LEFT JOIN commune co ON co.`id-commune`=c.`id-commune` LEFT JOIN address a ON a.`id-address`=d.`id-address` WHERE d.`id-device`=?", { replacements: [idDevice], type: QueryTypes.SELECT }
        );
        return infoEmailS;
    } catch (error) {
        throw error;
    }
};
querys.faultPending = async(id) => {
    try {
        if (!id) {
            const infoFault = await conn.query(
                "SELECT t.`name-type-output`, d.`name-device`,a.`name-address`, f.`id-crew`,f.`id-failure`, f.`fail-detect-date` FROM failure f LEFT JOIN device d ON d.`id-device`=f.`id-device` LEFT JOIN `type-output` t ON t.`id-type-output`=f.`type-fault` LEFT JOIN address a ON a.`id-address`=d.`id-address` WHERE f.`status-failure`=1", { type: QueryTypes.SELECT }
            );
            return infoFault;
        }
        if (id) {
            const infoFault = await conn.query(
                "SELECT t.`name-type-output`, d.`name-device`,a.`name-address`, f.`id-crew`,f.`id-failure`, f.`fail-detect-date` FROM failure f LEFT JOIN device d ON d.`id-device`=f.`id-device` LEFT JOIN `type-output` t ON t.`id-type-output`=f.`type-fault` LEFT JOIN address a ON a.`id-address`=d.`id-address` WHERE f.`status-failure`=1 AND f.`id-crew`= ?", { replacements: [parseInt(id)], type: QueryTypes.SELECT }
            );
            return infoFault;
        }
    } catch (error) {
        throw error;
    }
};

/*
 
  ######   ######## ########    #### ########      ######  ########  ######## ##      ##    ######## ##     ## ########  ##        #######  ##    ## ######## ######## 
 ##    ##  ##          ##        ##  ##     ##    ##    ## ##     ## ##       ##  ##  ##    ##       ###   ### ##     ## ##       ##     ##  ##  ##  ##       ##       
 ##        ##          ##        ##  ##     ##    ##       ##     ## ##       ##  ##  ##    ##       #### #### ##     ## ##       ##     ##   ####   ##       ##       
 ##   #### ######      ##        ##  ##     ##    ##       ########  ######   ##  ##  ##    ######   ## ### ## ########  ##       ##     ##    ##    ######   ######   
 ##    ##  ##          ##        ##  ##     ##    ##       ##   ##   ##       ##  ##  ##    ##       ##     ## ##        ##       ##     ##    ##    ##       ##       
 ##    ##  ##          ##        ##  ##     ##    ##    ## ##    ##  ##       ##  ##  ##    ##       ##     ## ##        ##       ##     ##    ##    ##       ##       
  ######   ########    ##       #### ########      ######  ##     ## ########  ###  ###     ######## ##     ## ##        ########  #######     ##    ######## ######## 
 
*/
querys.getIdCrew = async(id) => {
    try {
        if (id) {
            const idcrew = await conn.query(
                "SELECT `id-crew` FROM employee e  WHERE e.`id-user` =? ", { replacements: [id], type: QueryTypes.SELECT }
            );
            return idcrew;
        }
    } catch (error) {
        throw error;
    }
};
querys.crewInfoEmployees = async(communes) => {
    try {
        if (communes) {
            const getCrewXEmploy = await conn.query(
                "SELECT c.`id-crew`, e.`id-employee`, c.`name-crew`,co.`name-commune`,co.`id-commune`,e.`name-employee`,e.`lastname-employee`, e.`phone-employee`,r.`name-rol` from crew c LEFT JOIN employee e ON e.`id-crew`=c.`id-crew` LEFT JOIN commune co ON co.`id-commune`=c.`id-commune` LEFT JOIN user u ON u.`email-user`=e.`id-user` LEFT JOIN rol r ON r.`id-rol`=u.`id-rol` WHERE co.`id-commune`= ? ORDER BY c.`name-crew` ASC ", { replacements: [communes], type: QueryTypes.SELECT }
            );
            return getCrewXEmploy;
        }
        const getCrewXEmploy2 = await conn.query(
            "SELECT c.`id-crew`, e.`id-employee`, c.`name-crew`,co.`name-commune`,co.`id-commune`,e.`name-employee`,e.`lastname-employee`, e.`phone-employee`,r.`name-rol` from crew c LEFT JOIN employee e ON e.`id-crew`=c.`id-crew` LEFT JOIN commune co ON co.`id-commune`=c.`id-commune` LEFT JOIN user u ON u.`email-user`=e.`id-user` LEFT JOIN rol r ON r.`id-rol`=u.`id-rol` ORDER BY c.`name-crew` ASC ", { replacements: [communes], type: QueryTypes.SELECT }
        );
        return getCrewXEmploy2;
    } catch (error) {
        throw error;
    }
};

// querys.infoTypeFault = async(init, end) => {
//     try {
//         if (init && end) {
//             const getInfoTypeFault = await conn.query(
//                 "SELECT COUNT(`id-output`) Total, CAST(`fecha-output` AS DATE) Date, `type-output` from output WHERE CAST(`fecha-output` AS DATE) BETWEEN ? AND ?  GROUP BY CAST(`fecha-output` AS DATE),`type-output` ", { replacements: [init, end], type: QueryTypes.SELECT }
//             );
//             return getInfoTypeFault;
//         }
//         if (init === undefined && end === undefined) {
//             const getInfoTypeFault = await conn.query(
//                 "SELECT COUNT(`id-output`) Total, CAST(`fecha-output` AS DATE) Date, `type-output` from output GROUP BY CAST(`fecha-output` AS DATE),`type-output` ", { type: QueryTypes.SELECT }
//             );
//             return getInfoTypeFault;
//         }
//         if (init || end === undefined) {
//             const getInfoTypeFault = await conn.query(
//                 "SELECT COUNT(`id-output`) Total, CAST(`fecha-output` AS DATE) Date, `type-output` from output WHERE CAST(`fecha-output` AS DATE) >= ? GROUP BY CAST(`fecha-output` AS DATE),`type-output` ", { replacements: [init], type: QueryTypes.SELECT }
//             );
//             return getInfoTypeFault;
//         }
//         if (init === undefined || end) {
//             const getInfoTypeFault = await conn.query(
//                 "SELECT COUNT(`id-output`) Total, CAST(`fecha-output` AS DATE) Date, `type-output` from output WHERE CAST(`fecha-output` AS DATE) <= ? GROUP BY CAST(`fecha-output` AS DATE),`type-output` ", { replacements: [end], type: QueryTypes.SELECT }
//             );
//             return getInfoTypeFault;
//         }
//     } catch (error) {
//         throw error;
//     }
// };

querys.infoTypeFault = async(init, end) => {
    try {
        if (init && end) {
            const getInfoTypeFault = await conn.query(
                "SELECT COUNT(`id-failure`) Total, CAST(`fail-detect-date` AS DATE) Date, `type-fault` from failure WHERE CAST(`fail-detect-date` AS DATE) BETWEEN ? AND ? GROUP BY CAST(`fail-detect-date` AS DATE),`type-fault` ", { replacements: [init, end], type: QueryTypes.SELECT }
            );
            return getInfoTypeFault;
        }
        if (init === undefined && end === undefined) {
            const getInfoTypeFault = await conn.query(
                "SELECT COUNT(`id-failure`) Total, CAST(`fail-detect-date` AS DATE) Date, `type-fault` from failure GROUP BY CAST(`fail-detect-date` AS DATE),`type-fault` LIMIT 10", { type: QueryTypes.SELECT }
            );
            return getInfoTypeFault;
        }
        if (init || end === undefined) {
            const getInfoTypeFault = await conn.query(
                "SELECT COUNT(`id-failure`) Total, CAST(`fail-detect-date` AS DATE) Date, `type-fault` from failure WHERE CAST(`fail-detect-date` AS DATE) >= ? GROUP BY CAST(`fail-detect-date` AS DATE),`type-fault`", { replacements: [init], type: QueryTypes.SELECT }
            );
            return getInfoTypeFault;
        }
        if (init === undefined || end) {
            const getInfoTypeFault = await conn.query(
                "SELECT COUNT(`id-failure`) Total, CAST(`fail-detect-date` AS DATE) Date, `type-fault` from failure WHERE CAST(`fail-detect-date` AS DATE) <= ? GROUP BY CAST(`fail-detect-date` AS DATE),`type-fault` ", { replacements: [end], type: QueryTypes.SELECT }
            );
            return getInfoTypeFault;
        }
    } catch (error) {
        throw error;
    }
};
querys.typeFault = async(device) => {
    try {
        const typeError = await conn.query(
            "SELECT `normal-shunt-voltage`,`normal-current`,`normal-power` FROM device WHERE `id-device` = ?", { replacements: [device], type: QueryTypes.SELECT }
        );
        return typeError;
    } catch (error) {
        throw error;
    }
};
querys.info24Ago = async(fecha, idDevice) => {
    try {
        const info = await conn.query(
            "SELECT `fecha-output`, `current-output`, `power-output`, `shunt-voltage-output` from output WHERE `fecha-output` >= ? AND `id-device` = ? limit 60", { replacements: [fecha, idDevice], type: QueryTypes.SELECT }
        );
        return info;
    } catch (error) {
        throw error;
    }
};

querys.usersInfo = async() => {
    try {
        const info = await conn.query(
            "SELECT u.`email-user` email, r.`name-rol` rol, e.`name-employee` nombre, e.`lastname-employee` apellido, e.`phone-employee` telefono, e.`id-crew` crew, e.`title-employee` cargo, e.`born-date-employee` fecha FROM `user` u LEFT JOIN employee e ON e.`id-user`=u.`email-user` LEFT JOIN rol r ON r.`id-rol` = u.`id-rol` "
        );
        console.log(info);
        return info;
    } catch (error) {
        throw error;
    }
};

querys.getDateFaultFix = async(init, end) => {
    try {
        if (init && end) {
            const getDateFault = await conn.query(
                "SELECT `id-crew`,`fail-detect-date`, `fix-date` FROM failure WHERE CAST(`fail-detect-date` AS DATE) BETWEEN ? AND ?", { replacements: [init, end], type: QueryTypes.SELECT }
            );
            return getDateFault;
        }
        if (init === undefined && end === undefined) {
            const getDateFault = await conn.query(
                "SELECT `id-crew`,`fail-detect-date`, `fix-date` FROM failure", { type: QueryTypes.SELECT }
            );
            return getDateFault;
        }
        if (init || end === undefined) {
            const getDateFault = await conn.query(
                "SELECT `id-crew`,`fail-detect-date`, `fix-date` FROM failure WHERE CAST(`fail-detect-date` AS DATE) >= ?", { replacements: [init], type: QueryTypes.SELECT }
            );
            return getDateFault;
        }
        if (init === undefined || end) {
            const getDateFault = await conn.query(
                "SELECT `id-crew`,`fail-detect-date`, `fix-date` FROM failure WHERE CAST(`fail-detect-date` AS DATE) <= ?", { replacements: [end], type: QueryTypes.SELECT }
            );
            return getDateFault;
        }
    } catch (error) {
        throw error;
    }
};

querys.topFault = async(limite, init, end) => {
    try {
        if (limite) {
            if (init && end) {
                const top = await conn.query(
                    "SELECT d.`id-device` , COUNT(f.`id-failure`) numberFault, (SELECT COUNT(a.`id-failure`) FROM failure a WHERE a.`type-fault`=5 AND a.`id-device` =  d.`id-device`)'NIGHT-OFF', (SELECT COUNT(a.`id-failure`) FROM failure a WHERE a.`type-fault`=2 AND a.`id-device` =  d.`id-device` ) 'HIGH-USE', (SELECT COUNT(a.`id-failure`) FROM failure a WHERE a.`type-fault`=3 AND a.`id-device` =  d.`id-device`) 'LOW-USE',   (SELECT COUNT(a.`id-failure`) FROM failure a WHERE a.`type-fault`=4 AND a.`id-device` =  d.`id-device`) 'DAY-ON' FROM failure f RIGHT JOIN device d ON d.`id-device`=f.`id-device` WHERE CAST(`fail-detect-date` AS DATE) BETWEEN ? AND ? GROUP BY d.`id-device` ORDER BY COUNT(f.`id-failure`) DESC LIMIT ?", {
                        replacements: [init, end, parseInt(limite)],
                        type: QueryTypes.SELECT,
                    }
                );
                return top;
            }
            if (init === undefined && end === undefined) {
                const top = await conn.query(
                    "SELECT d.`id-device` , COUNT(f.`id-failure`) numberFault, (SELECT COUNT(a.`id-failure`) FROM failure a WHERE a.`type-fault`=5 AND a.`id-device` =  d.`id-device`)'NIGHT-OFF', (SELECT COUNT(a.`id-failure`) FROM failure a WHERE a.`type-fault`=2 AND a.`id-device` =  d.`id-device` ) 'HIGH-USE', (SELECT COUNT(a.`id-failure`) FROM failure a WHERE a.`type-fault`=3 AND a.`id-device` =  d.`id-device`) 'LOW-USE',   (SELECT COUNT(a.`id-failure`) FROM failure a WHERE a.`type-fault`=4 AND a.`id-device` =  d.`id-device`) 'DAY-ON' FROM failure f RIGHT JOIN device d ON d.`id-device`=f.`id-device`  GROUP BY d.`id-device` ORDER BY COUNT(f.`id-failure`) DESC LIMIT ?", { replacements: [parseInt(limite)], type: QueryTypes.SELECT }
                );
                return top;
            }
            if (init || end === undefined) {
                const top = await conn.query(
                    "SELECT d.`id-device` , COUNT(f.`id-failure`) numberFault, (SELECT COUNT(a.`id-failure`) FROM failure a WHERE a.`type-fault`=5 AND a.`id-device` =  d.`id-device`)'NIGHT-OFF', (SELECT COUNT(a.`id-failure`) FROM failure a WHERE a.`type-fault`=2 AND a.`id-device` =  d.`id-device` ) 'HIGH-USE', (SELECT COUNT(a.`id-failure`) FROM failure a WHERE a.`type-fault`=3 AND a.`id-device` =  d.`id-device`) 'LOW-USE',   (SELECT COUNT(a.`id-failure`) FROM failure a WHERE a.`type-fault`=4 AND a.`id-device` =  d.`id-device`) 'DAY-ON' FROM failure f RIGHT JOIN device d ON d.`id-device`=f.`id-device`  WHERE CAST(f.`fail-detect-date` AS DATE) >= ? GROUP BY d.`id-device` ORDER BY COUNT(f.`id-failure`) DESC LIMIT ?", { replacements: [init, parseInt(limite)], type: QueryTypes.SELECT }
                );
                return top;
            }
            if (init === undefined || end) {
                const top = await conn.query(
                    "SELECT d.`id-device` , COUNT(f.`id-failure`) numberFault, (SELECT COUNT(a.`id-failure`) FROM failure a WHERE a.`type-fault`=5 AND a.`id-device` =  d.`id-device`)'NIGHT-OFF', (SELECT COUNT(a.`id-failure`) FROM failure a WHERE a.`type-fault`=2 AND a.`id-device` =  d.`id-device` ) 'HIGH-USE', (SELECT COUNT(a.`id-failure`) FROM failure a WHERE a.`type-fault`=3 AND a.`id-device` =  d.`id-device`) 'LOW-USE',   (SELECT COUNT(a.`id-failure`) FROM failure a WHERE a.`type-fault`=4 AND a.`id-device` =  d.`id-device`) 'DAY-ON' FROM failure f RIGHT JOIN device d ON d.`id-device`=f.`id-device`   WHERE CAST(f.`fail-detect-date` AS DATE) <= ? GROUP BY d.`id-device` ORDER BY COUNT(f.`id-failure`) DESC LIMIT ?", { replacements: [end, parseInt(limite)], type: QueryTypes.SELECT }
                );
                return top;
            }
        }
        if (!limite) {
            if (init && end) {
                const top = await conn.query(
                    "SELECT `id-device` , COUNT(`id-failure`) numberFault FROM failure WHERE CAST(`fail-detect-date` AS DATE) BETWEEN ? AND ? GROUP BY `id-device` ORDER BY COUNT(`id-failure`) DESC", { replacements: [init, end], type: QueryTypes.SELECT }
                );
                return top;
            }
            if (init === undefined && end === undefined) {
                const top = await conn.query(
                    "SELECT `id-device` , COUNT(`id-failure`) numberFault FROM failure GROUP BY `id-device` ORDER BY COUNT(`id-failure`) DESC", { type: QueryTypes.SELECT }
                );
                return top;
            }
            if (init || end === undefined) {
                const top = await conn.query(
                    "SELECT `id-device` , COUNT(`id-failure`) numberFault FROM failure WHERE CAST(`fail-detect-date` AS DATE) >= ? GROUP BY `id-device` ORDER BY COUNT(`id-failure`) DESC", { replacements: [init], type: QueryTypes.SELECT }
                );
                return top;
            }
            if (init === undefined || end) {
                const top = await conn.query(
                    "SELECT `id-device` , COUNT(`id-failure`) numberFault FROM failure WHERE CAST(`fail-detect-date` AS DATE) <= ? GROUP BY `id-device` ORDER BY COUNT(`id-failure`) DESC", { replacements: [end], type: QueryTypes.SELECT }
                );
                return top;
            }
        }
    } catch (error) {
        throw error;
    }
};
querys.getInfoGeneral = async(init, end) => {
    try {
        if (init && end) {
            const infoGeneral = await conn.query(
                "SELECT  CAST(o.`fecha-output` as DATE) fecha, SUM(o.`current-output`) corriente, SUM(o.`shunt-voltage-output`) voltaje, SUM(o.`power-output`) potencia FROM output o LEFT JOIN device d ON o.`id-device` = d.`id-device` LEFT JOIN address a ON a.`id-address`=d.`id-address` LEFT JOIN commune c ON c.`id-commune`=a.`id-commune` WHERE CAST(o.`fecha-output` as DATE) BETWEEN ? AND ? GROUP BY CAST(o.`fecha-output` as DATE)", { replacements: [init, end], type: QueryTypes.SELECT }
            );
            return infoGeneral;
        }
        if (init === undefined && end === undefined) {
            const infoGeneral = await conn.query(
                "SELECT  CAST(o.`fecha-output` as DATE) fecha, SUM(o.`current-output`) corriente, SUM(o.`shunt-voltage-output`) voltaje, SUM(o.`power-output`) potencia FROM output o LEFT JOIN device d ON o.`id-device` = d.`id-device` LEFT JOIN address a ON a.`id-address`=d.`id-address` LEFT JOIN commune c ON c.`id-commune`=a.`id-commune` GROUP BY CAST(o.`fecha-output` as DATE)", { type: QueryTypes.SELECT }
            );
            return infoGeneral;
        }
        if (init || end === undefined) {
            const infoGeneral = await conn.query(
                "SELECT  CAST(o.`fecha-output` as DATE) fecha, SUM(o.`current-output`) corriente, SUM(o.`shunt-voltage-output`) voltaje, SUM(o.`power-output`) potencia FROM output o LEFT JOIN device d ON o.`id-device` = d.`id-device` LEFT JOIN address a ON a.`id-address`=d.`id-address` LEFT JOIN commune c ON c.`id-commune`=a.`id-commune` WHERE CAST(o.`fecha-output` as DATE) >= ? GROUP BY CAST(o.`fecha-output` as DATE)", { replacements: [init], type: QueryTypes.SELECT }
            );
            return infoGeneral;
        }
        if (init === undefined || end) {
            const infoGeneral = await conn.query(
                "SELECT  CAST(o.`fecha-output` as DATE) fecha, SUM(o.`current-output`) corriente, SUM(o.`shunt-voltage-output`) voltaje, SUM(o.`power-output`) potencia FROM output o LEFT JOIN device d ON o.`id-device` = d.`id-device` LEFT JOIN address a ON a.`id-address`=d.`id-address` LEFT JOIN commune c ON c.`id-commune`=a.`id-commune` WHERE CAST(o.`fecha-output` as DATE) <= ? GROUP BY CAST(o.`fecha-output` as DATE)", { replacements: [end], type: QueryTypes.SELECT }
            );
            return infoGeneral;
        }
    } catch (error) {
        throw error;
    }
};

querys.getCrewCommune = async(communes) => {
    try {
        const crewCommune = await conn.query(
            "SELECT `id-crew` from crew WHERE `id-commune` = ?", { replacements: [communes], type: QueryTypes.SELECT }
        );
        return crewCommune;
    } catch (error) {
        throw error;
    }
};

querys.getCrewCommune = async(communes) => {
    try {
        const crewCommune = await conn.query(
            "SELECT `id-crew` from crew WHERE `id-commune` = ?", { replacements: [communes], type: QueryTypes.SELECT }
        );
        return crewCommune;
    } catch (error) {
        throw error;
    }
};

querys.reportFails = async(init, end) => {
    try {
        let where = "";
        let stringQuery =
            "SELECT f.`id-failure` id, d.`id-device` idDevice, d.`name-device` nameDevice,co.`id-commune` idCommune, co.`name-commune` nameCommune, d.`lat-device` latDevice, d.`lon-device` lonDevice,d.working, f.`status-failure` statusId, sf.`name-status-failure` statusName, f.`employee-in-charge` employeeId, concat(e.`name-employee` ,' ', e.`lastname-employee`) employeeName, f.`id-crew` crewId, c.`name-crew` crewName, f.`fail-detect-date` detectDate, f.`fix-date` fixDate, f.`type-fault` faultId, tyo.`name-type-output` faultName FROM failure f  LEFT JOIN `status-failures` sf ON sf.`id-status-failure` = f.`status-failure` LEFT JOIN `employee` e ON e.`id-employee`=f.`employee-in-charge` LEFT JOIN `crew` c ON c.`id-crew`=f.`id-crew` LEFT JOIN `type-output` AS tyo ON tyo.`id-type-output`=f.`type-fault` LEFT JOIN `device` d ON  d.`id-device`= f.`id-device` LEFT JOIN `address` a ON d.`id-address`= a.`id-address` LEFT JOIN `commune` co ON co.`id-commune`=a.`id-commune` ";
        if (init && end)
            where = " WHERE f.`fail-detect-date` BETWEEN :init AND :end";
        if (init && !end) where = " WHERE f.`fail-detect-date` >= :init ";
        if (end && !init) where = " WHERE f.`fail-detect-date` <= :end";

        const consolidadoFallas = await conn.query(stringQuery + where, {
            replacements: { init, end },
            type: QueryTypes.SELECT,
        });
        return consolidadoFallas;
    } catch (error) {
        throw error;
    }
};

querys.reportOutputs = async(init, end) => {
    try {
        let where = "";
        let stringQuery =
            "SELECT o.`id-output` id, d.`id-device` idDevice, d.`name-device` nameDevice,co.`id-commune` idCommune, co.`name-commune` nameCommune, d.working,o.`fecha-output` `DATE`,o.`current-output` AS `current`, o.`shunt-voltage-output` voltage, o.`power-output` AS  `POWER`, o.`type-output` typeId, tyo.`name-type-output` typeName, d.`lat-device` latDevice, d.`lon-device` lonDevice FROM output o LEFT JOIN device d ON d.`id-device`=o.`id-device` LEFT JOIN `address` a ON d.`id-address`= a.`id-address`  LEFT JOIN `commune` co ON co.`id-commune`=a.`id-commune` LEFT JOIN `type-output` tyo ON tyo.`id-type-output`=o.`type-output`";
        if (init && end) where = " WHERE o.`fecha-output` BETWEEN :init AND :end";
        if (init && !end) where = " WHERE o.`fecha-output` >= :init ";
        if (end && !init) where = " WHERE o.`fecha-output` <= :end";

        const reponseTime = await conn.query(stringQuery + where, {
            replacements: { init, end },
            type: QueryTypes.SELECT,
        });
        return reponseTime;
    } catch (error) {
        throw error;
    }
};

querys.devicesXcommune = async(init, end) => {
    try {
        let where = "";
        let stringQuery =
            "SELECT c.`name-commune` COMUNA, COUNT(d.`id-device`) TOTAL, SUM(IF(d.working=1,1,0)) ACTIVO,SUM(IF(d.working=0,1,0)) CONFALLAS FROM device d RIGHT JOIN address a ON a.`id-address`=d.`id-address`  RIGHT JOIN commune c ON c.`id-commune`=a.`id-commune`";

        if (init && end)
            where = " WHERE CAST(d.`created-at` AS DATE) BETWEEN :init AND :end";
        if (init && !end) where = " WHERE CAST(d.`created-at` AS DATE) >= :init ";
        if (end && !init) where = " WHERE CAST(d.`created-at` AS DATE) <= :end";
        where += " GROUP BY c.`name-commune`";

        const devicesommune = await conn.query(stringQuery + where, {
            replacements: { init, end },
            type: QueryTypes.SELECT,
        });
        return devicesommune;
    } catch (error) {
        throw error;
    }
};

querys.outputxtype = async(init, end) => {
    try {
        let where = "";
        let stringQuery =
            "SELECT tp.`name-type-output` tipo, COUNT(o.`id-output`) cuenta FROM output o RIGHT JOIN `type-output` tp ON tp.`id-type-output`=o.`type-output`";

        if (init && end)
            where = " WHERE CAST(o.`fecha-output` AS DATE) BETWEEN :init AND :end";
        if (init && !end) where = " WHERE CAST(o.`fecha-output` AS DATE) >= :init ";
        if (end && !init) where = " WHERE CAST(o.`fecha-output` AS DATE) <= :end";
        where += " GROUP BY tp.`name-type-output`";

        const infoType = await conn.query(stringQuery + where, {
            replacements: { init, end },
            type: QueryTypes.SELECT,
        });
        return infoType;
    } catch (error) {
        throw error;
    }
};

querys.ResponseTimeRange = async(init, end) => {
    try {
        let where = "";
        let stringQuery =
            "SELECT f.`id-crew` CREW, MIN( TIMESTAMPDIFF(SECOND, f.`fail-detect-date`, f.`fix-date`) ) MINIMO, MAX(TIMESTAMPDIFF(SECOND, f.`fail-detect-date`, f.`fix-date`)) MAXIMO,AVG(TIMESTAMPDIFF(SECOND, f.`fail-detect-date`, f.`fix-date`)) PROMEDIO, COUNT(*) NUMFALLOS FROM failure f LEFT JOIN crew c ON c.`id-crew`=f.`id-crew` ";
        if (init && end)
            where =
            " WHERE cast(f.`fail-detect-date` AS DATE)  BETWEEN :init AND :end";
        if (init && !end)
            where = " WHERE cast(f.`fail-detect-date` AS DATE)  >= :init ";
        if (end && !init)
            where = "  WHERE cast(f.`fail-detect-date` AS DATE)  <= :end";
        where += "  GROUP BY f.`id-crew` ";

        const consolidadoOutputs = await conn.query(stringQuery + where, {
            replacements: { init, end },
            type: QueryTypes.SELECT,
        });
        return consolidadoOutputs;
    } catch (error) {
        throw error;
    }
};
querys.getFault = async(init, end) => {
    try {
        if (init && end) {
            const fault = await conn.query(
                "SELECT c.`id-commune` id, c.`name-commune` nombre, c.`lat-commune` lat, c.`lon-commune` lon, COUNT(f.`id-failure`) totalFallas from commune c left join address a ON a.`id-commune` = c.`id-commune` LEFT JOIN device d ON d.`id-address`=a.`id-address` LEFT JOIN failure f ON f.`id-device`=d.`id-device` WHERE CAST(f.`fail-detect-date` AS DATE) BETWEEN ? AND ? GROUP BY c.`id-commune`, c.`name-commune`, c.`lat-commune`, c.`lon-commune`", { replacements: [init, end], type: QueryTypes.SELECT }
            );
            return fault;
        }
        if (init === undefined && end === undefined) {
            const fault = await conn.query(
                "SELECT c.`id-commune` id, c.`name-commune` nombre, c.`lat-commune` lat, c.`lon-commune` lon, COUNT(f.`id-failure`) totalFallas from commune c left join address a ON a.`id-commune` = c.`id-commune` LEFT JOIN device d ON d.`id-address`=a.`id-address` LEFT JOIN failure f ON f.`id-device`=d.`id-device` GROUP BY c.`id-commune`, c.`name-commune`, c.`lat-commune`, c.`lon-commune`", { type: QueryTypes.SELECT }
            );
            return fault;
        }
        if (init || end === undefined) {
            const fault = await conn.query(
                "SELECT c.`id-commune` id, c.`name-commune` nombre, c.`lat-commune` lat, c.`lon-commune` lon, COUNT(f.`id-failure`) totalFallas from commune c left join address a ON a.`id-commune` = c.`id-commune` LEFT JOIN device d ON d.`id-address`=a.`id-address` LEFT JOIN failure f ON f.`id-device`=d.`id-device` WHERE CAST(f.`fail-detect-date` AS DATE) >= ? GROUP BY c.`id-commune`, c.`name-commune`, c.`lat-commune`, c.`lon-commune`", { replacements: [init], type: QueryTypes.SELECT }
            );
            return fault;
        }
        if (init === undefined || end) {
            const fault = await conn.query(
                "SELECT c.`id-commune` id, c.`name-commune` nombre, c.`lat-commune` lat, c.`lon-commune` lon, COUNT(f.`id-failure`) totalFallas from commune c left join address a ON a.`id-commune` = c.`id-commune` LEFT JOIN device d ON d.`id-address`=a.`id-address` LEFT JOIN failure f ON f.`id-device`=d.`id-device` WHERE CAST(f.`fail-detect-date` AS DATE) <= ? GROUP BY c.`id-commune`, c.`name-commune`, c.`lat-commune`, c.`lon-commune`", { replacements: [end], type: QueryTypes.SELECT }
            );
            return fault;
        }
    } catch (error) {
        throw error;
    }
};

querys.working = async(deviceId) => {
    try {
        const work = await conn.query(
            "SELECT `working` FROM device WHERE `id-device`= ?", { replacements: [deviceId], type: QueryTypes.SELECT }
        );
        return work;
    } catch (error) {
        throw error;
    }
};

querys.getIdEmployee = async(email) => {
    try {
        const employee = await conn.query(
            "SELECT `id-employee` FROM employee WHERE `id-user`= ?", { replacements: [email], type: QueryTypes.SELECT }
        );
        return employee;
    } catch (error) {
        throw error;
    }
};
querys.statusFailure = async(idFailure) => {
    try {
        const status = await conn.query(
            "SELECT `status-failure`, `id-device` FROM failure WHERE `id-failure`= ?", { replacements: [idFailure], type: QueryTypes.SELECT }
        );
        return status;
    } catch (error) {
        throw error;
    }
};

//---------------------UPDATE SQL----------------
querys.workUpdateOff = async(deviceId) => {
    try {
        const workingOff = await conn.query(
            "UPDATE device SET working= 0 where `id-device`= ?", { replacements: [deviceId], type: QueryTypes.UPDATE }
        );
    } catch (error) {
        throw error;
    }
};
querys.workUpdateOn = async(deviceId) => {
    try {
        const workingOn = await conn.query(
            "UPDATE device SET working= 1 where `id-device`= ?", { replacements: [deviceId], type: QueryTypes.UPDATE }
        );
    } catch (error) {
        throw error;
    }
};

querys.updateFailure = async(failureId, email) => {
    try {
        let date = new Date();
        const update = await conn.query(
            "UPDATE failure SET `status-failure`= 2,`employee-in-charge`= ?, `fix-date`=?  where `id-failure`= ?", { replacements: [email, date, failureId], type: QueryTypes.UPDATE }
        );
    } catch (error) {
        throw error;
    }
};

querys.change_KeyPassword = async(user, key) => {
    try {
        const update = await conn.query(
            "UPDATE user SET `password-user`= ? where `email-user`= ?", { replacements: [key, user], type: QueryTypes.UPDATE }
        );
    } catch (error) {
        throw error;
    }
};

module.exports = querys;