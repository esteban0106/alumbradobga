const Sequelize = require("sequelize");

const conn = require("../database/conectar_db");

const CityModel = require("../models/city_model");

const CommuneModel = conn.define(
    "commune", {
        "id-commune": {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            allowNull: false,
            primaryKey: true,
        },
        "name-commune": {
            type: Sequelize.STRING,
            allowNull: false,
        },

        // 'id-city': {
        //   type: Sequelize.INTEGER,
        //   allowNull: false,

        // },
    }, {
        //------- NO AGREGA EL CREATEAT Y UPDATEAT --------------------//
        timestamps: false,
        //------- EVITA QUE CAMBIE EL NOMBRE DE LA TABLA --------------//
        freezeTableName: true,
        //------- NOMBRE DE LA TABLA EN LA BASE DE DATOS --------------//
        tableName: "commune",
    }
);

// Relación 1:N entre Commune y City
CommuneModel.belongsTo(CityModel, {
    foreignKey: "id-city",
    constraints: false,
});
CityModel.hasMany(CommuneModel, { foreignKey: "id-city", constraints: false });

module.exports = CommuneModel;