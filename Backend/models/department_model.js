const Sequelize = require("sequelize");

const conn = require("../database/conectar_db");

const DepartmentModel = conn.define(
    "department", {
        "id-department": {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            allowNull: false,
            primaryKey: true,
        },

        "name-department": {
            type: Sequelize.STRING,
            allowNull: false,
        },
    }, {
        //------- NO AGREGA EL CREATEAT Y UPDATEAT --------------------//
        timestamps: false,
        //------- EVITA QUE CAMBIE EL NOMBRE DE LA TABLA --------------//
        freezeTableName: true,
        //------- NOMBRE DE LA TABLA EN LA BASE DE DATOS --------------//
        tableName: "department",
    }
);

module.exports = DepartmentModel;