const Sequelize = require("sequelize");

const conn = require("../database/conectar_db");
const AddressModel = require("../models/address_model");

const DeviceModel = conn.define(
    "device", {
        "id-device": {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            allowNull: false,
            primaryKey: true,
        },

        "name-device": {
            type: Sequelize.STRING,
            allowNull: false,
        },

        "lat-device": {
            type: Sequelize.DOUBLE,
            allowNull: false,
        },

        "lon-device": {
            type: Sequelize.DOUBLE,
            allowNull: false,
        },

        "description-luminary": {
            type: Sequelize.STRING,
        },

        "normal-shunt-voltage": {
            type: Sequelize.DOUBLE,
            allowNull: false,
        },

        "normal-current": {
            type: Sequelize.DOUBLE,
            allowNull: false,
        },

        "normal-power": {
            type: Sequelize.DOUBLE,
            allowNull: false,
        },
        working: {
            type: Sequelize.BOOLEAN,
            allowNull: false,
        },
        "created-at": {
            type: Sequelize.DATE,
        },
        "created-by": {
            type: Sequelize.STRING,
            allowNull: false,
        },
        "updated-at": {
            type: Sequelize.DATE,
        },
        "updated-by": {
            type: Sequelize.STRING,
            allowNull: false,
        },
        crew: {
            type: Sequelize.INTEGER,
            allowNull: false,
        },
    }, {
        //------- AGREGA EL CREATEAT Y UPDATEAT --------------------//
        timestamps: false,
        //------- EVITA QUE CAMBIE EL NOMBRE DE LA TABLA --------------//
        freezeTableName: true,
        //------- NOMBRE DE LA TABLA EN LA BASE DE DATOS --------------//
        tableName: "device",
    }
);

// Relación 1:N entre address y device
DeviceModel.belongsTo(AddressModel, {
    foreignKey: "id-address",
    constraints: false,
});
AddressModel.hasMany(DeviceModel, {
    foreignKey: "id-address",
    constraints: false,
});

module.exports = DeviceModel;