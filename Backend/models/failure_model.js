const Sequelize = require("sequelize");

const conn = require("../database/conectar_db");

const EmployeeModel = require("../models/employee_model");
const CrewModel = require("../models/crew_model");
const StatusFailuresModel = require("../models/status_failures_model");
const DeviceModel = require("../models/device_model");
const FailureModel = conn.define(
    "failure", {
        "id-failure": {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            allowNull: false,
            primaryKey: true,
        },

        // 'status-failure': {
        //   type: Sequelize.INTEGER,
        //   allowNull: false,
        // },

        // 'employee-in-charge': Sequelize.INTEGER,
        // 'id-device': {
        //   type: Sequelize.INTEGER,
        //   allowNull: false,
        // },

        "fail-detect-date": {
            type: Sequelize.DATE,
            allowNull: true,
        },
        "fix-date": Sequelize.DATE,
        // 'id-crew': Sequelize.INTEGER,
        "type-fault": Sequelize.INTEGER,
    }, {
        //------- AGREGA EL CREATEAT Y UPDATEAT --------------------//
        timestamps: false,
        //------- EVITA QUE CAMBIE EL NOMBRE DE LA TABLA --------------//
        freezeTableName: true,
        //------- NOMBRE DE LA TABLA EN LA BASE DE DATOS --------------//
        tableName: "failure",
    }
);

// Relación 1:N entre Failure y Employee
FailureModel.belongsTo(EmployeeModel, {
    foreignKey: "employee-in-charge",
    constraints: false,
});
EmployeeModel.hasMany(FailureModel, {
    foreignKey: "employee-in-charge",
    constraints: false,
});

// Relación 1:N entre Failure y StatusFailure
FailureModel.belongsTo(StatusFailuresModel, {
    foreignKey: "id-status-failure",
    constraints: false,
});
StatusFailuresModel.hasMany(FailureModel, {
    foreignKey: "status-failure",
    constraints: false,
});

// Relación 1:N entre Failure y StatusFailure
FailureModel.belongsTo(DeviceModel, {
    foreignKey: "id-device",
    constraints: false,
}); //1 User puede tener 1 rol
DeviceModel.hasMany(FailureModel, {
    foreignKey: "id-device",
    constraints: false,
});

// Relación 1:N entre Failure y Crew
FailureModel.belongsTo(CrewModel, {
    foreignKey: "id-crew",
    constraints: false,
});
CrewModel.hasMany(FailureModel, { foreignKey: "id-crew", constraints: false });

module.exports = FailureModel;