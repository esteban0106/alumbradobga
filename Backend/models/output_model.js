const Sequelize = require("sequelize");

const conn = require("../database/conectar_db");
const DeviceModel = require("../models/device_model");
const TypeOutputModel = require("../models/type_output_model");

const OutputModel = conn.define(
    "output", {
        "id-output": {
            type: Sequelize.BIGINT,
            autoIncrement: true,
            allowNull: false,
            primaryKey: true,
        },

        // "id-device": {
        //     type: Sequelize.INTEGER,
        //     allowNull: false,
        // },
        "fecha-output": {
            type: Sequelize.DATE,
            allowNull: true,
        },

        "current-output": {
            type: Sequelize.DOUBLE,
            allowNull: false,
        },
        "shunt-voltage-output": {
            type: Sequelize.DOUBLE,
            allowNull: false,
        },

        "power-output": {
            type: Sequelize.DOUBLE,
            allowNull: false,
        },
        // "type-output": {
        //     type: Sequelize.INTEGER,
        //     allowNull: false,
        // },
    }, {
        //------- AGREGA EL CREATEAT Y UPDATEAT --------------------//
        timestamps: false,
        //------- EVITA QUE CAMBIE EL NOMBRE DE LA TABLA --------------//
        freezeTableName: true,
        //------- NOMBRE DE LA TABLA EN LA BASE DE DATOS --------------//
        tableName: "output",
    }
);

// Relación 1:N entre output y device
OutputModel.belongsTo(DeviceModel, {
    foreignKey: "id-device",
    constraints: false,
});
DeviceModel.hasMany(OutputModel, {
    foreignKey: "id-device",
    constraints: false,
});

// Relación 1:N entre output y device
OutputModel.belongsTo(TypeOutputModel, {
    foreignKey: "id-type-output",
    constraints: false,
});
TypeOutputModel.hasMany(OutputModel, {
    foreignKey: "type-output",
    constraints: false,
});

module.exports = OutputModel;