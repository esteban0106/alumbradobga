const Sequelize = require('sequelize');

const conn = require('../database/conectar_db');

const RolModel = conn.define('rol', {

  // --------------- DEFINIMOS LOS ATRIBUTOS -----------------
  'id-rol': {
    type: Sequelize.INTEGER,
    autoIncrement: true,
    allowNull: false,
    primaryKey: true,

  },
  'name-rol': {
    type: Sequelize.STRING,
    allowNull: false,
  },
}, {

  //------- NO AGREGA EL CREATEAT Y UPDATEAT --------------------//
  timestamps: false,
  //------- EVITA QUE CAMBIE EL NOMBRE DE LA TABLA --------------//
  freezeTableName: true,
  //------- NOMBRE DE LA TABLA EN LA BASE DE DATOS --------------//
  tableName: 'rol'
});

module.exports = RolModel;