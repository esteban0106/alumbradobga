const express = require("express");
const cors = require("cors");
const conn = require("../database/conectar_db");
const bodyParser = require("body-parser");
const path = require("path");
class Server {
    syncDB() {
        conn
            .sync()
            .then((resp) => {
                console.log("====== BASE DE DATOS SINCRONIZADA ======");
            })
            .catch((err) => console.log(err));
    }
    constructor() {
        this.app = express();
        this.port = process.env.PORT;

        //Middlewares
        this.middlewares();

        //Rutas
        this.routes();

        //sincronizar base de datos
        this.syncDB();
    }

    middlewares() {
        // parse application/x-www-form-urlencoded
        this.app.use(bodyParser.urlencoded({ extended: true }));

        // parse application/json
        this.app.use(bodyParser.json());

        //cors
        this.app.use(cors());

        // Lectura y parseo del body
        this.app.use(express.json());

        //directorio publico
        this.app.use(express.static("public_html"));
    }

    routes() {
        this.app.use(require("../routes/routes"));
        this.app.get("*", (req, res) => {
            res.sendFile(path.resolve(__dirname, "../public_html/index.html"));
        });
    }

    listen() {
        this.app.listen(this.port, () => {
            console.log("funcionando en puerto: ", this.port);
        });
    }
}

module.exports = Server;