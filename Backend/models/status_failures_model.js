const Sequelize = require("sequelize");

const conn = require("../database/conectar_db");

const StatusFailuresModel = conn.define(
    "status-failures", {
        "id-status-failure": {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            allowNull: false,
            primaryKey: true,
        },

        "name-status-failure": {
            type: Sequelize.STRING,
            allowNull: false,
        },
    }, {
        //------- NO AGREGA EL CREATEAT Y UPDATEAT --------------------//
        timestamps: false,
        //------- EVITA QUE CAMBIE EL NOMBRE DE LA TABLA --------------//
        freezeTableName: true,
        //------- NOMBRE DE LA TABLA EN LA BASE DE DATOS --------------//
        tableName: "status-failures",
    }
);

module.exports = StatusFailuresModel;