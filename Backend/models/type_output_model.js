const Sequelize = require("sequelize");

const conn = require("../database/conectar_db");

const TypeOutputModel = conn.define(
    "type-output", {
        "id-type-output": {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            allowNull: false,
            primaryKey: true,
        },

        "name-type-output": {
            type: Sequelize.STRING,
            allowNull: false,
        },
        priority: {
            type: Sequelize.INTEGER,
            allowNull: false,
        },
    }, {
        //------- NO AGREGA EL CREATEAT Y UPDATEAT --------------------//
        timestamps: false,
        //------- EVITA QUE CAMBIE EL NOMBRE DE LA TABLA --------------//
        freezeTableName: true,
        //------- NOMBRE DE LA TABLA EN LA BASE DE DATOS --------------//
        tableName: "type-output",
    }
);

module.exports = TypeOutputModel;