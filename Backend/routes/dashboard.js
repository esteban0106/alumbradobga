const { Router } = require("express");
const {
    faultCommune,
    getInfoQueryDate,
    topDeviceFault,
    FaultFix,
    crewCommune,
    infoLast24Hours,
    typeFault,
    crewXcommune,
    pendingXcrew,
    reportFault,
    responseTime,
    devicesXcommune,
    outputxtype
} = require("../controllers/dashboard_controller");
const { validatorJWT } = require("../middlewares/jwt-validator");

const router = Router();

//------------get---------------------
//------------post---------------------
router.post("/pendingXcrew", [validatorJWT], pendingXcrew);
router.post("/crewXcommune", [validatorJWT], crewXcommune);
router.post("/infoLast24Hours", [validatorJWT], infoLast24Hours);
router.post("/infoTypeOutput", [validatorJWT], typeFault);
router.post("/crewCommune", [validatorJWT], crewCommune);
router.post("/faultFix", [validatorJWT], FaultFix);
router.post("/fault", [validatorJWT], faultCommune);
router.post("/infoDate", [validatorJWT], getInfoQueryDate);
router.post("/topDevice", [validatorJWT], topDeviceFault);
router.post("/reportFault", [validatorJWT], reportFault);
router.post("/responseTime", [validatorJWT], responseTime);
router.post("/devicesXcommune", [validatorJWT], devicesXcommune);
router.post("/outputxtype", [validatorJWT], outputxtype);

//------------put---------------------

//------------delete---------------------

module.exports = router;