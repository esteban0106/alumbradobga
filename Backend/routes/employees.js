const { Router } = require("express");
const employee = require("../controllers/employees_controller");
const { validatorJWT } = require("../middlewares/jwt-validator");

const router = Router();

//------------get---------------------
router.get("/getUsers", [validatorJWT],employee.getInfoUser);
router.get("/:id", [validatorJWT],employee.getEmployeeById);

//------------post---------------------
router.post("/",[validatorJWT], employee.createEmployee);
router.post("/crew", [validatorJWT],employee.createCrew);

//------------put---------------------
router.put("/:id", [validatorJWT],employee.updateEmploye);
router.put("/changeCrew/:id", [validatorJWT],employee.changeEmployeeCrew);

//------------delete---------------------

module.exports = router;