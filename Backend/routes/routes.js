const express = require("express");

const app = express();

app.use("/api/employees", require("./employees"));
app.use("/api/devices", require("./devices"));
app.use("/api/auth", require("./auth"));
app.use("/api/failure", require("./failure"));
app.use("/api/dashboard", require("./dashboard"));

app.use((error, request, response, next) => {
    response.status(error.status || 500).json({
        status: "error",
        error: {
            message: error.message || serverErrorMsg,
        },
    });
});

module.exports = app;