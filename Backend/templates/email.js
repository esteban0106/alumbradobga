"use strict";

const nodemailer = require("nodemailer");
require("dotenv").config();

this.enviar_mail = (pnombre, info) => {
    let transporter = nodemailer.createTransport({
        host: "email-smtp.us-east-1.amazonaws.com",
        port: 587,
        secure: false,
        auth: {
            user: process.env.AWSUSER,
            pass: process.env.AWSPASS,
        },
    });

    let mail_options = {
        from: "alumbradobga@gmail.com",
        to: pnombre,
        subject: "Bienvenido a la aplicación",
        html: `
            <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="x-apple-disable-message-reformatting"> 
    <title>Welcome - [Plain HTML]</title> 
        <link href="https://fonts.googleapis.com/css?family=Montserrat:300,500" rel="stylesheet">
    
    <style>

       
        html,
        body {
            margin: 0 auto !important;
            padding: 0 !important;
            height: 100% !important;
            width: 100% !important;
        }

        * {
            -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: 100%;
        }

        div[style*="margin: 16px 0"] {
            margin:0 !important;
        }

        table,
        td {
            mso-table-lspace: 0pt !important;
            mso-table-rspace: 0pt !important;
        }

        table {
            border-spacing: 0 !important;
            border-collapse: collapse !important;
            table-layout: fixed !important;
            margin: 0 auto !important;
        }
        table table table {
            table-layout: auto;
        }

        img {
            -ms-interpolation-mode:bicubic;
        }

        *[x-apple-data-detectors],  /* iOS */
        .x-gmail-data-detectors,    /* Gmail */
        .x-gmail-data-detectors *,
        .aBn {
            border-bottom: 0 !important;
            cursor: default !important;
            color: inherit !important;
            text-decoration: none !important;
            font-size: inherit !important;
            font-family: inherit !important;
            font-weight: inherit !important;
            line-height: inherit !important;
        }

        .a6S {
            display: none !important;
            opacity: 0.01 !important;
        }
        img.g-img + div {
            display:none !important;
           }

        .button-link {
            text-decoration: none !important;
        }

        
        @media only screen and (min-device-width: 375px) and (max-device-width: 413px) { /* iPhone 6 and 6+ */
            .email-container {
                min-width: 375px !important;
            }
        }

    </style>

    <style>

        .button-td,
        .button-a {
            transition: all 100ms ease-in;
        }
        .button-td:hover,
        .button-a:hover {
            background: #000000 !important;
            border-color: #000000 !important;
        }

        @media screen and (max-width: 480px) {

            .fluid {
                width: 100% !important;
                max-width: 100% !important;
                height: auto !important;
                margin-left: auto !important;
                margin-right: auto !important;
            }

            .stack-column,
            .stack-column-center {
                display: block !important;
                width: 100% !important;
                max-width: 100% !important;
                direction: ltr !important;
            }
            .stack-column-center {
                text-align: center !important;
            }

            .center-on-narrow {
                text-align: center !important;
                display: block !important;
                margin-left: auto !important;
                margin-right: auto !important;
                float: none !important;
            }
            table.center-on-narrow {
                display: inline-block !important;
            }

            .email-container p {
                font-size: 17px !important;
                line-height: 22px !important;
            }
        }

    </style>


    <xml>
        <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch></o:PixelsPerInch>
        </o:OfficeDocumentSettings>
    </xml>

</head>
<body width="100%" bgcolor="#F1F1F1" style="margin: 0; mso-line-height-rule: exactly;">
    <center style="width: 100%; background: #F1F1F1; text-align: left;">

        <div style="display:none;font-size:1px;line-height:1px;max-height:0px;max-width:0px;opacity:0;overflow:hidden;mso-hide:all;font-family: sans-serif;">
        </div>

       
        <div style="max-width: 680px; margin: auto;" class="email-container">
            <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="680" align="center">
            <tr>
            <td>

            <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="100%" style="max-width: 680px;" class="email-container">


                <tr>
                    <td bgcolor="#333333">
                        <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tr>
                                <td style="padding: 30px 40px 30px 40px; text-align: center;">
                               <p style="font-family: sans-serif; font-size:14px; line-height:20px; color:#666666; padding:0; margin:0;" class="body-text">NOTIFICACIÓN DE FALLAS</p>  
                                </td>
                            </tr>
                        </table>
                        
                    </td>
                </tr>

                <tr>
                    
                    <td background="background.jpg" bgcolor="#222222" align="center" valign="top" style="text-align: center; background-position: center center !important; background-size: cover !important;">
                        <v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style="width:680px; height:380px; background-position: center center !important;">
                        <v:fill type="tile" src="../img/background.jpg" color="#222222" />
                        <v:textbox inset="0,0,0,0">
                        <div>
                            <table role="presentation" border="0" cellspacing="0" cellpadding="0" align="center" width="500">
                            <tr>
                            <td align="center" valign="middle" width="500">
                            <table role="presentation" border="0" cellpadding="0" cellspacing="0" align="center" width="100%" style="max-width:500px; margin: auto;">

                                <tr>
                                    <td height="20" style="font-size:20px; line-height:20px;">&nbsp;</td>
                                </tr>

                                <tr>
                                  <td align="center" valign="middle">
                                    
                                  <table>
                                     <tr>
                                         <td valign="top" style="text-align: center; padding: 60px 0 10px 20px;">
                                     
                                             <h1 style="margin: 0; font-family: 'Montserrat', sans-serif; font-size: 30px; line-height: 36px; color: #ffffff; font-weight: bold;">Cuadrilla ${info[0]["name-crew"]} </h1>
                                         </td>
                                     </tr>
                                     <tr>
                                         <td valign="top" style="text-align: center; padding: 10px 20px 15px 20px; font-family: sans-serif; font-size: 18px; line-height: 20px; color: #ffffff;">
                                             <p style="margin: 0;">En estos momentos el dispotivo ${info[0]["name-device"]} nos ha notificacido de una falla técnica.</p>
                                         </td>
                                     </tr>
                                     <tr>
                                         <td valign="top" align="center" style="text-align: center; padding: 15px 0px 60px 0px;">

                                             <center>
                                             <table role="presentation" align="center" cellspacing="0" cellpadding="0" border="0" class="center-on-narrow" style="text-align: center;">
                                                 <tr>
                                                     <td style="border-radius: 50px; background: #0159B7; text-align: center;" class="button-td">
                                                     </td>
                                                 </tr>
                                             </table>
                                             </center>

                                         </td>
                                     </tr> 
                                  </table>

                                  </td>
                                </tr>
                            
                                <tr>
                                    <td height="20" style="font-size:20px; line-height:20px;">&nbsp;</td>
                                </tr>

                            </table>
                            </td>
                            </tr>
                            </table>
                        </div>
                        </v:textbox>
                        </v:rect>
                        <![endif]-->
                    </td>
                </tr>
                <!-- HERO : END -->

                <!-- INTRO : BEGIN -->
                <tr>
                    <td bgcolor="#ffffff">
                        <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tr>
                                <td style="padding: 40px 40px 20px 40px; text-align: left;">
                                    <h1 style="margin: 0; font-family: 'Montserrat', sans-serif; font-size: 20px; line-height: 26px; color: #333333; font-weight: bold;">Se requiere de la asistencia inmediata en la zona
                                    </h1>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 0px 40px 20px 40px; font-family: sans-serif; font-size: 18px; line-height: 20px; color: #555555; text-align: left; font-weight:bold;">
                                    <p style="margin: 0;">
                                    	Comuna: ${info[0]["name-commune"]}<br>
                                        Dirección: ${info[0]["name-address"]}<br>
                                        idDevice: ${info[0]["id-device"]}<br>
                                        Longitud: ${info[0]["lon-device"]}<br>
                                        Latitud: ${info[0]["lat-device"]}<br>
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 0px 40px 20px 40px; font-family: sans-serif; font-size: 18px; line-height: 20px; color: #555555; text-align: left; font-weight:normal;">
                                    <p style="margin: 0;">Por favor informar al Jefe de cuadrilla una vez el trabajo se haya realizado.</p>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 0px 40px 20px 40px; font-family: sans-serif; font-size: 18px; line-height: 20px; color: #555555; text-align: left; font-weight:normal;">
                                    <p style="margin: 0;">Muchas gracias.</p>
                                </td>
                            </tr>

                            <tr>
                                <td align="left" style="padding: 0px 40px 40px 40px;">

                                    <table width="180" align="left">
                                        <tr>
                                          <td width="110">
                                            <table width="" cellpadding="0" cellspacing="0" border="0">
                                              <tr>
                                                <td align="left" style="font-family: sans-serif; font-size:14px; line-height:20px; color:#222222; font-weight:bold;" class="body-text">
                                                  <p style="font-family: 'Montserrat', sans-serif; font-size:14px; line-height:20px; color:#222222; font-weight:bold; padding:0; margin:0;" class="body-text">${info[0]["name-device"]}</p>   
                                                </td>               
                                              </tr>
                                              <tr>
                                                <td align="left" style="font-family: sans-serif; font-size:14px; line-height:20px; color:#666666;" class="body-text">
                                                  <p style="font-family: sans-serif; font-size:14px; line-height:20px; color:#666666; padding:0; margin:0;" class="body-text">AlumbradoBGA</p>    
                                                </td>               
                                              </tr>                            
                                            </table>

                                          </td>
                                        </tr>
                                    </table>

                                </td>
                            </tr>

                        </table>
                    </td>
                </tr>
                <!-- INTRO : END -->

                <!-- CTA : BEGIN -->
                <tr>
                    <td bgcolor="#0159B7">
                        <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tr>
                                <td style="padding: 40px 40px 5px 40px; text-align: center;">
                                    <h1 style="margin: 0; font-family: 'Montserrat', sans-serif; font-size: 20px; line-height: 24px; color: #ffffff; font-weight: bold;">AlumbradoBGA</h1>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 0px 40px 20px 40px; font-family: sans-serif; font-size: 17px; line-height: 23px; color: #aad4ea; text-align: center; font-weight:normal;">
                                    <p style="margin: 0;">Soluciones efectivas al alumbrado público.</p>
                                </td>
                            </tr>
                            <tr>
                                <td valign="middle" align="center" style="text-align: center; padding: 0px 20px 40px 20px;">

                                    <!-- Button : BEGIN -->
                                    <table role="presentation" align="center" cellspacing="0" cellpadding="0" border="0" class="center-on-narrow">
                                        <tr>
                                            <td style="border-radius: 50px; background: #ffffff; text-align: center;" class="button-td">
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- Button : END -->

                                </td>
                            </tr>

                        </table>
                    </td>
                </tr>
                <!-- CTA : END -->

                <!-- SOCIAL : BEGIN -->
                <tr>
                    <td bgcolor="#292828">
                        <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tr>
                                <td style="padding: 30px 30px; text-align: center;">
                                </td>
                            </tr>

                        </table>
                    </td>
                </tr>
                <!-- SOCIAL : END -->

                <!-- FOOTER : BEGIN -->
                <tr>
                    <td bgcolor="#ffffff">
                        <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tr>
                                <td style="padding: 40px 40px 10px 40px; font-family: sans-serif; font-size: 12px; line-height: 18px; color: #666666; text-align: center; font-weight:normal;">
                                    <p style="margin: 0;">alumbradobga@gmail.com</p>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 0px 40px 10px 40px; font-family: sans-serif; font-size: 12px; line-height: 18px; color: #666666; text-align: center; font-weight:normal;">
                                    <p style="margin: 0;">Este Email fué enviado de forma automática desde el dispositivo</p>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 0px 40px 40px 40px; font-family: sans-serif; font-size: 12px; line-height: 18px; color: #666666; text-align: center; font-weight:normal;">
                                    <p style="margin: 0;">Copyright &copy; 2021 <b>AlumbradoBGA Company</b>, All Rights Reserved.</p>
                                </td>
                            </tr>

                        </table>
                    </td>
                </tr>

            </table>

            </td>
            </tr>
            </table>
        </div>

    </center>
</body>
        `,
    };
    transporter.sendMail(mail_options, (error, info) => {
        if (error) {
            console.log(error);
        } else {
            console.log("El correo se envío correctamente "); //+ info.response);
        }
    });
};

module.export = this;