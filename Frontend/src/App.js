import React from 'react'
import { AppRouter } from './routers/AppRouter'
import { Provider } from 'react-redux'
import { store } from './store/store'
import { createTheme } from '@material-ui/core'
import { ThemeProvider } from '@material-ui/styles'

export const App = () => {
const theme = createTheme();
    
    return (
        <Provider store={store}>
<ThemeProvider theme={theme}>
            <AppRouter />
            </ThemeProvider>
        </Provider>
    )
}
