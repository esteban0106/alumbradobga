import Swal from "sweetalert2";
import { devicesTypes } from "../types/devicesTypes";
import { httpToken } from "../utils/fetch";
import { finishLoading, startLoading } from "./uiActions";



export const startGetDevices = (comuna, idDev) => {
    return async (dispatch) => {
      let query=''
      if(idDev){
        query = (idDev)?`?idDev=${idDev} `:'';

      }else{
        if(comuna) 
        query = (comuna)?`?commune=${comuna} `:'';

      }
  
      try {
      dispatch(startLoading() )

        const resp = await httpToken(`devices/${query}`);
        const data = await resp.json();
        dispatch(finishLoading() )

        if (data.body) {
          dispatch(getDevices(data.body.rows));
        } else {
  
          Swal.fire('Error',JSON.stringify( data.message? data.message : data.error?.message), 'error');
  
        }
      } catch (e) {
        dispatch(finishLoading() )

        Swal.fire('Error', `${e}`, 'error');
  
      }
  
    }

};

export const startAddDevices = (body) => {
  return async (dispatch) => {

   
    try {
      dispatch(startLoading() )

      const resp = await httpToken(`devices`, body, 'POST');
      const data = await resp.json();
      dispatch(finishLoading() )

      if (data.body) {
        dispatch(addDevices(data.body));
      } else {

        Swal.fire('Error',JSON.stringify( data.message? data.message : data.error?.message), 'error');

      }
    } catch (e) {
      dispatch(finishLoading() )

      Swal.fire('Error', `${e}`, 'error');


    }

  }

};

export const startGetLast24 = (id) => {
  return async (dispatch) => {

   
    try {
      dispatch(startLoading() )

      const resp = await httpToken(`dashboard/infoLast24Hours`, {idDevice:id}, 'POST');
      const data = await resp.json();
      dispatch(finishLoading() )

      if (data.ok) {
        dispatch(last24(data.body===0?[]:data.body));
      } else {

        Swal.fire('Error',JSON.stringify( data.message? data.message : data.error?.message), 'error');

      }
    } catch (e) {
      dispatch(finishLoading() )

      Swal.fire('Error', `${e}`, 'error');


    }

  }

};

export const startGetCrew = (comuna) => {
  return async (dispatch) => {

   
    try {
      dispatch(startLoading() )

      const resp = await httpToken(`dashboard/crewCommune`, {communes:comuna}, 'POST');
      const data = await resp.json();
      dispatch(finishLoading() )

      if (data.ok) {
        dispatch(getCrew(data.body));
      } else {

        Swal.fire('Error',JSON.stringify( data.message? data.message : data.error?.message), 'error');

      }
    } catch (e) {
      dispatch(finishLoading() )

      Swal.fire('Error', `${e}`, 'error');


    }

  }

};
export const addClient = (client)=>({
  type: devicesTypes.addClient,
  payload: client

});

export const getCrew = (crew)=>({
  type: devicesTypes.loadCrew,
  payload: crew

});
export const addValuesToDevices = (values)=>({
  type: devicesTypes.addValuesDevices,
  payload: values

});

export const resetDevices = ()=>({
  type: devicesTypes.resetDevices,

});
const addDevices = (device)=>({
  type: devicesTypes.addDevices,
  payload: device

})

const last24 = (fails)=>({
  type: devicesTypes.addLast24,
  payload: fails

})
//  "name-device": "LUM-123",
// "lat-device": 7.105955414287668,
// "lon-device": -73.1142080730143,
// "description-luminary": "LUMINARIA PARQUE",
// "normal-shunt-voltage": 0.95,
// "normal-current": 2,
// "normal-power": 0.15,
// "working":true,
// "created-by":"mafevera",
// "updated-by":"mafevera",
// "address":{ "name-address": "Provenza", "id-commune": 18 }
const getDevices = (devices)=>({
    type: devicesTypes.devicesLoad,
    payload: devices

})

export const getIdDevice = (id)=>({
  type: devicesTypes.selectIdDevice,
  payload: id

})
  
