import Swal from "sweetalert2";
import { globalTypes } from "../types/globalTypes";

import { httpToken } from "../utils/fetch";
import { startChecking } from "./authActions";
import { finishLoading, startLoading } from "./uiActions";

export const startLoadingCommunas = () =>{
    return async (dispatch) => {
  
     
      try {
      dispatch(startLoading() )

        const resp = await httpToken(`devices/getComunas`, 'GET');
        const data = await resp.json();
      dispatch(finishLoading() )

        if (data.ok) {

          dispatch(loadComunas(data.body));
        } else {
  
          Swal.fire('Error',JSON.stringify( data.message? data.message : data.error?.message), 'error');
  
        }
      } catch (e) {
      dispatch(finishLoading() )

        Swal.fire('Error', `${e}`, 'error');
  
  
      }
    }
  
  }
  
  const loadComunas = (comunas)=>({
    type: globalTypes.globalLoadCommune,
    payload: comunas
  
  })