const mqtt = require("mqtt");

const clientId = "alumbradoMar_" + Math.random().toString(16).substr(2, 8);
const host = "ws://alumbradobga.ga:8093/mqtt";
//No funciona con 8094 porque esta corriendo localmente
const options = {
    keepalive: 60,
    clientId: clientId,
    username: "web_client",
    password: "private",
    protocolId: "MQTT",
    protocolVersion: 4,
    clean: true,
    reconnectPeriod: 1000,
    connectTimeout: 3 * 1000,
    will: {
        topic: "WillMsg",
        payload: "Connection Closed abnormally..!",
        qos: 0,
        retain: false,
    },
};
export const mqttfun = () =>{

console.log("Connecting mqtt client");

const client = mqtt.connect(host, options);
client.on("connect", () => {
    console.log("Mqtt connect with WS! Congratulations!!");
});
client.on("error", (err) => {
    console.log("Connection error: ", err);
    client.end();
});

client.on("reconnect", () => {
    console.log("Reconnecting...");
});


return client;
}
export const mqttSuscribe = (client) =>{
    client.on("connect", () => {
        console.log("Mqtt connect with WS! Congratulations!!");
        client.subscribe("values", { qos: 0 }, (error) => {
            if (!error) {
                console.log("subscription Successful");
            } else {
                console.log("subscription failed");
            }
        });
    });

   
}

export const mqttDisconnect = (client) =>{

  if(client.connected){

      client.end(() => {
          console.log('Mqtt disconnect!');
        });
  }
}


