// dashboard/crewXcommune

import Swal from "sweetalert2";
import { crewsTypes } from "../types/crewTypes";
import { userType } from "../types/userTypes";
import { httpToken } from "../utils/fetch";
import { finishLoading, startLoading } from "./uiActions";


export const startGetUsers = () => {
    return async (dispatch) => {
  
      
      try {
      dispatch(startLoading() )

          
        const resp = await httpToken(`employees/getUsers`);
        const data = await resp.json();
      dispatch(finishLoading() )

        if (resp.ok) {
          dispatch(loadUsers(data.body));
        } else {
  
          Swal.fire('Error',JSON.stringify( data.message? data.message : data.error?.message), 'error');
  
        }
      } catch (e) {
      dispatch(finishLoading() )

        Swal.fire('Error', `${e}`, 'error');
  
      }
  
    }

};

export const startAddUser = (body) => {
    return async (dispatch) => {
  
      
      try {
        dispatch(startLoading() )


        const resp = await httpToken(`auth/signUp`,body, 'POST');
        const data = await resp.json();
        dispatch(finishLoading() )


        if (resp.ok) {
            dispatch(loadNewUser(data.user['email-user']));
            Swal.fire(
              'Confirmado',
              'Usuario creado',
              'success'
            )
        } else {
  
          Swal.fire('Error',JSON.stringify( data.message? data.message : data.error?.message), 'error');
  
        }
      } catch (e) {
        dispatch(finishLoading() )

        Swal.fire('Error', `${e}`, 'error');
  
      }
  
    }

};

export const startAddEmployee = (body) => {
  return async (dispatch) => {

    
    try {
        dispatch(startLoading() )

        const resp = await httpToken(`employees`,body, 'POST');
        const data = await resp.json();
        dispatch(finishLoading() )


        if (resp.ok) {

            dispatch(loadNewEmployee(true));
            Swal.fire(
              'Confirmado',
              'Empleado creado',
              'success'
            )
        } else {
  
          Swal.fire('Error',JSON.stringify( data.message? data.message : data.error?.message), 'error');
  
        }
      } catch (e) {
        dispatch(finishLoading() )

        Swal.fire('Error', `${e}`, 'error');
  
      }
  }

};
 const loadUsers = (users)=>({
    type: userType.loadUsers,
    payload: users
  
  })

  export const loadNewUser = (iduser)=>({
    type: userType.crearUsuario,
    payload: iduser
  
  });

  export const loadNewEmployee = (created)=>({
    type: userType.crearEmpleado,
    payload: created

  
  })

  export const resetUser = ()=>({
    type: userType.resetUser,

  
  })
  
  