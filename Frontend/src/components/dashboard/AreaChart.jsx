import React from 'react'
import ReactApexChart from 'react-apexcharts';
import { useSelector } from 'react-redux';

export const AreaChart = () => {
  const {devicesComuna} = useSelector(state => state.dashboard)

    const state = {
      
        series: [
            {  name: 'Dispositivos con fallas',
       data: devicesComuna ?devicesComuna.map(item => item.CONFALLAS +''):[],},
          {
            name: 'Dispositivos activos',
          data: devicesComuna ?devicesComuna.map(item => item.ACTIVO +''):[],
          },
          
        { name: 'Dispositivos totales',
        data: devicesComuna ?devicesComuna.map(item => item.TOTAL+''):[],}
        ],
        options: {
            title: {
                text: 'Dispositivos por comuna'
              },
          chart: {
            type: 'area',
            height: 350,
            stacked: false,
            
            // events: {
            //   selection: function (chart, e) {
            //     console.log(new Date(e.xaxis.min))
            //   }
            // },
          },
          colors: ['#008FFB', '#00E396', '#FEB019'],
          dataLabels: {
            enabled: false
          },
          stroke: {
            curve: 'smooth'
          },
          fill: {
            type: 'gradient',
            gradient: {
              opacityFrom: 0.5,
              opacityTo: 0.6,
            }
          },
          yaxis:{
            labels: {
              
                formatter: function (val, i) {
                  return val + ' '
                }
              },
          },
        
          legend: {
            position: 'top',
            horizontalAlign: 'left'
          },
          xaxis: {
            maxWidth: 1,
            

            labels: {
                formatter: function (val, i) {
                  return `${val}` + ' '
                }
              },
            categories: devicesComuna ?devicesComuna.map(item => item.COMUNA):[],
          },
        },
      
      
      };

    return (
        <div id="chart">
        <ReactApexChart options={state.options} series={state.series} type="area" height={350} />
        </div>
    )
}




