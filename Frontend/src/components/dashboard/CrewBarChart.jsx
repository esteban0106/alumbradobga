
import React from 'react'
import ReactApexChart from 'react-apexcharts';
import { useSelector } from 'react-redux';

export const CrewBarChart = () => {

  const {respTime} = useSelector(state => state.dashboard);

   const state = {
      
        series: [{
          name: 'Tiempo',
          data: respTime.map(item => item['performance']),
        }],
        options: {
      
          chart: {
          
            height: 350,
            type: 'bar',
           
          },
          dataLabels: {
            enabled: true
          },
      
          title: {
            text: 'Tiempo de respuesta por cuadrilla'
          },
          markers: {
            size: 4,
            colors: ['#fff'],
            strokeColor: '#FF4560',
            strokeWidth: 2,
          },
          tooltip: {
            y: {
              formatter: function(val) {
                return `${val} s` // convertir a horas
              }
            }
          },
          xaxis: {
            categories: respTime.map(item =>{  return `Cuadrilla ${item['idCrew']}`}),
        
          },
          yaxis: {
            tickAmount: 7,
            labels: {
              formatter: function(val, i) {
               return val + ' s'
              }
            }
          }
        },
      
      
      };
    return (
        <div id="chart">
        <ReactApexChart options={state.options} series={state.series} type="bar" height={350} />
        </div>
    )
}
