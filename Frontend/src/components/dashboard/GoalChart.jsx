import React from 'react'
import ReactApexChart from 'react-apexcharts';
import { useSelector } from 'react-redux';

export const GoalChart = () => {
  const { respTime } = useSelector(state => state.dashboard);

  const state = {

    series: [
      {
        name: 'Tiempo de respuesta promedio',
        data: respTime ? respTime.map(item => {
          return {
            x: item.CREW.toString(),
            y: item.PROMEDIO,
            goals: [
              {
                name: 'Mejor tiempo de respuesta',
                value: item.MINIMO,
                strokeWidth: 5,
                strokeColor: '#775DD0'
              }
              ,
              {
                name: 'Peor tiempo de respuesta',
                value: item.MAXIMO,
                strokeWidth: 5,
                strokeColor: '#FEB019'
              }
            ]
          }
        }) : []
      }],
    //     [
    //       {
    //         x: 
    //         y: respTime? respTime.map(item => ) :[],

    //         ]
    //       },
    //       {
    //         x: '2012',
    //         y: 4432,
    //         goals: [
    //           {
    //             name: 'Expected',
    //             value: 5400,
    //             strokeWidth: 5,
    //             strokeColor: '#775DD0'
    //           }
    //         ]
    //       },
    //       {
    //         x: '2013',
    //         y: 5423,
    //         goals: [
    //           {
    //             name: 'Expected',
    //             value: 5200,
    //             strokeWidth: 5,
    //             strokeColor: '#775DD0'
    //           }
    //         ]
    //       },
    //       {
    //         x: '2014',
    //         y: 6653,
    //         goals: [
    //           {
    //             name: 'Expected',
    //             value: 6500,
    //             strokeWidth: 5,
    //             strokeColor: '#775DD0'
    //           }
    //         ]
    //       },
    //       {
    //         x: '2015',
    //         y: 8133,
    //         goals: [
    //           {
    //             name: 'Expected',
    //             value: 6600,
    //             strokeWidth: 5,
    //             strokeColor: '#775DD0'
    //           }
    //         ]
    //       },
    //       {
    //         x: '2016',
    //         y: 7132,
    //         goals: [
    //           {
    //             name: 'Expected',
    //             value: 7500,
    //             strokeWidth: 5,
    //             strokeColor: '#775DD0'
    //           }
    //         ]
    //       },
    //       {
    //         x: '2017',
    //         y: 7332,
    //         goals: [
    //           {
    //             name: 'Expected',
    //             value: 8700,
    //             strokeWidth: 5,
    //             strokeColor: '#775DD0'
    //           }
    //         ]
    //       },
    //       {
    //         x: '2018',
    //         y: 6553,
    //         goals: [
    //           {
    //             name: 'Expected',
    //             value: 7300,
    //             strokeWidth: 5,
    //             strokeColor: '#775DD0'
    //           }
    //         ]
    //       }
    //     ]
    //   }
    // ],
    options: {
      chart: {
       
        height: 350,
        type: 'bar'
      },
      tooltip:{
    fixed: {
                    enabled: true,
                    position: 'topRight', // topRight, topLeft, bottomRight, bottomLeft
                    offsetY: 30,
                    offsetX: 60
                },
      },
      yaxis: {
        axisTicks: {
          show: true,
      },

        labels: {
          formatter: function (val, i) {
            return val + ' s'
          }
        }
      },
      xaxis: {
        axisTicks: {
          show: true,
      },
   
        categories: respTime ? respTime.map(item => item.CREW):[],
        type: 'category',
        labels: {
          formatter: function (val, i) {
            
            if ((val % 2) === 0 || (val % 2) === 1) {
              return 'Cuadrilla ' + val
            } else {
              return ''
            }
          }
        }
      },
      plotOptions: {
        bar: {
          columnWidth: '60%'
        }
      },
      colors: ['#00E396'],
      dataLabels: {
        enabled: false
      },
      legend: {
        show: true,
        showForSingleSeries: true,
        customLegendItems: ['Tiempo promedio de respuesta', 'Mejor tiempo de respuesta', 'Peor tiempo de respuesta'],
        markers: {
          fillColors: ['#00E396', '#775DD0', '#FEB019']
        }
      }
    },


  };


  return (

    <div id="chart">
      <h4>Tiempo de respuesta por cuadrilla</h4>
      <ReactApexChart options={state.options} series={state.series} type="bar" height={350} />
    </div>

  )
}
