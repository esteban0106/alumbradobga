
import moment from 'moment';
import React from 'react';
import ReactApexChart from 'react-apexcharts';
import { useSelector } from 'react-redux';


export const LineCharts = () => {

  const { CVPTotal } = useSelector(state => state.dashboard);
  // const time = CVPTotal.map(item => moment(new Date(item.fecha)).format('X'))
  const time = CVPTotal.map(item => item.fecha)

  // console.log(time[0])
  // const time=[1,2,3,4]
  const corriente = CVPTotal.map(item => item.corriente);
  const voltaje = CVPTotal.map(item => item.voltaje);
  const potencia = CVPTotal.map(item => item.potencia);

  const state = {

    series: [{
      name: "Corriente",

      data: corriente
    }],
    options: {
      markers: {
        size: 5,
      },
      tooltip: {
        enabled: true,
      },

      zoom: {
        type: 'x',
        enabled: true,
        autoScaleYaxis: true
      },
      toolbar: {
        autoSelected: 'zoom'
      },
      //   // categories: time,
      xaxis: {
        categories: time,


      },
      title: {
        text: 'Corriente',

        style: {
          fontSize: '14px',
          fontWeight: 'bold',
          fontFamily: 'Open Sans',
          color: '#8f5bd9'
        },
      },
      chart: {
        id: 'fb',
        group: 'social',
        type: 'line',
        height: 200
      },
      colors: ['#0cbce4'],
      yaxis: {
        show: true,
        labels: {
          minWidth: 40,
          formatter: function (value, timestamp, opts) {


            return Math.round(value)

          }
        }
      }
    },

    seriesLine2: [{
      name: "Voltaje",
      data: voltaje
    }],
    optionsLine2: {
      markers: {
        size: 5,
      },
      zoom: {
        type: 'x',
        enabled: true,
        autoScaleYaxis: true
      },
      toolbar: {
        autoSelected: 'zoom'
      },
      tooltip: {
        enabled: true,
      },
      title: {
        text: 'Voltaje',

        style: {
          fontSize: '14px',
          fontWeight: 'bold',
          fontFamily: 'Open Sans',
          color: '#8f5bd9'
        },
      },
      chart: {
        id: 'tw',
        group: 'social',
        type: 'line',
        height: 200
      },
      colors: ['#4c3073'],
      yaxis: {
        show: true,
        labels: {
          minWidth: 40,
          formatter: function (value, timestamp, opts) {

            return Math.round(value)

          }
        }
      },
      //   // categories: time,
      xaxis: {
        categories: time,


      },
    },

    seriesLine3: [{
      name: "Potencia",

      data: potencia
    }],
    optionsLine3: {
      markers: {
        size: 5,
      },
      zoom: {
        type: 'x',
        enabled: true,
        autoScaleYaxis: true
      },
      toolbar: {
        autoSelected: 'zoom'
      },
      tooltip: {
        enabled: true,
      },
      title: {
        text: 'Potencia',

        style: {
          fontSize: '14px',
          fontWeight: 'bold',
          fontFamily: 'Open Sans',
          color: '#8f5bd9'
        },
      },
      chart: {
        id: 'pw',
        group: 'social',
        type: 'line',
        height: 200
      },
      colors: ['#f2522e'],
      yaxis: {
        show: true,
        labels: {
          minWidth: 40,
          formatter: function (value, timestamp, opts) {

            return Math.round(value)

          }
        }
      },
      //   // categories: time,
      xaxis: {
        categories: time,


      },
    },


  }


  return (

    <div id="wrapper">
      <div id="chart-line">
        <ReactApexChart options={state.options} series={state.series} type="line" height={200} />
      </div>
      <div id="chart-line2">
        <ReactApexChart options={state.optionsLine2} series={state.seriesLine2} type="line" height={200} />
      </div>
      <div id="chart-area">
        <ReactApexChart options={state.optionsLine3} series={state.seriesLine3} type="line" height={200} />
      </div>
    </div>
  )
}

