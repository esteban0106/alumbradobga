import ReactApexChart from "react-apexcharts";
import { useSelector } from "react-redux";

  




export const MixedBarChart = () => {
  const {topDevices} = useSelector(state => state.dashboard);

    const state = {
      
        series: [{
          name: 'Fallos por dispositivos',
          type: 'column',
          data: topDevices? topDevices.map(item => item['numberFault']):[]
        }, {
          name: 'Variación estandar',
          type: 'line',
          data: topDevices?topDevices.map(item => item['numberFault']/100):[]
        }
      ],
        options: {
          chart: {
            height: 350,
            type: 'line',
          },
          stroke: {
            width: [0, 4]
          },
          title: {
            text: 'Fallos por dispositivo'
          },
          dataLabels: {
            enabled: true,
            enabledOnSeries: [1]
          },
          labels: topDevices?topDevices.map(item => item['id-device']):[] ,
          xaxis: {
            type: 'category'
          },
          yaxis: [
              {
            title: {
              text: '# de fallos',
            },
          
          },
           {
            opposite: true,
            title: {
              text: 'Desviación'
            }
          }]
        },
      
      
      };
    return (
        <div id="chart">
<ReactApexChart options={state.options} series={state.series} type="line" height={350} />
</div>
    )
}
