import React from 'react'
import ReactApexChart from 'react-apexcharts';
import { useSelector } from 'react-redux';

export const MultiChart = () => {
  const {topDevices} = useSelector(state => state.dashboard);

    const state = {

        series: [{
            name: 'HIGH-USE',
            type: 'column',
            data: topDevices? topDevices.map(item => item['HIGH-USE']+''):[]
        }, {
            name: 'LOW-USE',
            type: 'column',
            data:  topDevices? topDevices.map(item => item['LOW-USE']+''):[]
        }, 
        {
            name: 'DAY-ON',
            type: 'column',
            data: topDevices? topDevices.map(item => item['DAY-ON'] +''):[]
        },
        {
            name: 'NIGHT-OFF',
            type: 'column',
            data: topDevices? topDevices.map(item => item['NIGHT-OFF']+''):[]
        },
        {
            name: 'TOTAL',
            type: 'line',
            data: topDevices? topDevices.map(item => item['numberFault']+''):[]
        }],
        options: {
            chart: {
                height: 350,
                type: 'line',
                stacked: false,
                dropShadow: {
                    enabled: true,
                    color: '#000',
                    top: 18,
                    left: 7,
                    blur: 10,
                    opacity: 0.2
                  },
            },
            dataLabels: {
                enabled:true,
                enabledOnSeries: [4]
            },
            stroke: {
                width: [1, 1, 4]
            },
            title: {
                text: 'Dispositivos con más fallos',
                align: 'left',
                offsetX: 110
            },
            xaxis: {
                categories: topDevices? topDevices.map(item => item['id-device']):[],
                labels: {
                    formatter: function(val, i) {
                        return 'Dispositivo ' + val
                     
                        
                    }
                  }
            },
            yaxis: 
                {
                  
                    axisTicks: {
                        show: false,
                    },
                    axisBorder: {
                        show: false,
                    },
                 
                    title: {
                        text: "# de fallos",
                      
                    },
                
                },
                // {

                //     show: false

                // },
                // { show: false},
                // { show: false},
                // {
                //     seriesName: 'Revenue',
                //     show: false,
                // dataLabels: {
                //     enabled: true,
                //   },

                //   grid: {
                //     padding: {
                //       left: 50,
                //       right: 50
                //     }
                //   },
                //     axisTicks: {
                //         show: false,
                //     },
                //     axisBorder: {
                //         show: true,
                //         color: '#FEB019'
                //     },
                //     labels: {
                //         style: {
                //             colors: '#FEB019',
                //         },
                //     },
                //     title: {
                //         text: "Total de fallos",
                //         style: {
                //             color: '#FEB019',
                //         }
                //     }
                // },
            
            tooltip: {
                // fixed: {
                //     enabled: true,
                //     position: 'topLeft', // topRight, topLeft, bottomRight, bottomLeft
                //     offsetY: 30,
                //     offsetX: 60
                // },
            },
            legend: {
                horizontalAlign: 'center',
                offsetX: 40
            }
        },


    };


    return (
        <div id="chart">
            <ReactApexChart  options={state.options} series={state.series} type="line" height={350} />
        </div>
    )
}

