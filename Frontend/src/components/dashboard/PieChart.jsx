import React from 'react'
import ReactApexChart from 'react-apexcharts';
import { useSelector } from 'react-redux';

export const PieChart = () => {
  const { failsComuna } = useSelector(state => state.dashboard)
  const state = {

    series: failsComuna.map(item => item.totalFallas),
    options: {
   
      plotOptions: {
        pie: {
          size: 200
        }
      },
      chart: {
        width: '100%',
        type: 'pie',
        padding: 0
      },
      labels: failsComuna.map(item => item.nombre),
      colors: [
        "#f98404",
        "#b24775",
        "#7896d2",
        "#9fe6a0",
        "#ffdf9a",
        "#ff8d73",
        "#ffc5c9",
        "#73c5ea",
        "#ffd248",
        "#fe5377",
        "#aebfeb",
        "#d98ae3",
        "#ffb256",
        "#01c8e9",
        "#debbd7",
        "#511281",
        "#f55c47"
      ],
      // theme: {
      //   monochrome: {
      //     enabled: true
      //   }
      // },
    
      title: {
        text: "Fallos por localidad"
      },
      dataLabels: {
        formatter(val, opts) {
          const name = opts.w.globals.labels[opts.seriesIndex]
          return [name, val.toFixed(1) + '%']
        }
      },
      legend: {
        show: true,
        position: 'bottom',
      }
    }







  }
  return (
      
      failsComuna.length !== 0 &&
      <ReactApexChart options={state.options} series={state.series} type="pie" height="100%"/>
    
  )
}
