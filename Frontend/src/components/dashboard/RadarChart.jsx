
import React from 'react'
import ReactApexChart from 'react-apexcharts';
import { useSelector } from 'react-redux';

export const RadarChart = () => {

  const {outputType} = useSelector(state => state.dashboard)


   const state = {
      
        series: [{
          name:'Tipos de salidas',
          data:outputType? outputType.map(item => item.cuenta ) :[],
        }],
        options: {
          chart: {
            height: 350,
            type: 'radar',
          },
          dataLabels: {
            enabled: true
          },
          plotOptions: {
            radar: {
              size: 140,
              polygons: {
                strokeColors: '#e9e9e9',
                fill: {
                  colors: ['#f8f8f8', '#fff']
                }
              }
            }
          },
          title: {
            text: 'Tipos de registros'
          },
          colors: ['#FF4560','#775DD0','#FEB019'],
          markers: {
            size: 4,
            colors: ['#fff'],
            strokeColor: '#FF4560',
            strokeWidth: 2,
          },
          tooltip: {
            y: {
              formatter: function(val) {
                return val
              }
            }
          },
          yaxis:{
            
            labels: {
              formatter: function(val, i) {
               return val +''
              }
            }
          },
          xaxis: {
            categories:outputType? outputType.map(item => item.tipo ) :[],
           
            labels: {
              formatter: function(val, i) {
               return val +''
              }
            }
        
          },
        
        },
      
      
      };
    return (
        <div id="chart">
        <ReactApexChart options={state.options} series={state.series} type="radar" height={350} />
        </div>
    )
}
