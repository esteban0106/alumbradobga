import React from 'react'
import ReactApexChart from 'react-apexcharts'
import { useSelector } from 'react-redux';

export const RangeChart = () => {
  const {respTime} = useSelector(state => state.dashboard);

   const  state = {
      
        series: [{
            data: respTime?respTime.map(item =>[item.MINIMO, item.MAXIMO +1]
              
                
            ):[]
                // {
                //   x: '2011',
                //   y: [1292,1500],
                //   goals: [
                //     {
                //       name: 'Expected',
                //       value: 1400,
                //       strokeWidth: 5,
                //       strokeColor: '#775DD0'
                //     }
                //   ]
                // },
                // {
                //   x: '2012',
                //   y: [4432, 5000],
                //   goals: [
                //     {
                //       name: 'Expected',
                //       value: 5400,
                //       strokeWidth: 5,
                //       strokeColor: '#775DD0'
                //     }
                //   ]
                // },
                // {
                //   x: '2013',
                //   y: [5423, 6000],
                //   goals: [
                //     {
                //       name: 'Expected',
                //       value: 5200,
                //       strokeWidth: 5,
                //       strokeColor: '#775DD0'
                //     }
                //   ]
                // },
                // {
                //   x: '2014',
                //   y: [6653, 7000],
                //   goals: [
                //     {
                //       name: 'Expected',
                //       value: 6500,
                //       strokeWidth: 5,
                //       strokeColor: '#775DD0'
                //     }
                //   ]
                // },
                // {
                //   x: '2015',
                //   y: [8133, 9000],
                //   goals: [
                //     {
                //       name: 'Expected',
                //       value: 6600,
                //       strokeWidth: 5,
                //       strokeColor: '#775DD0'
                //     }
                //   ]
                // },
                // {
                //   x: '2016',
                //   y: [7132, 7500],
                //   goals: [
                //     {
                //       name: 'Expected',
                //       value: 7500,
                //       strokeWidth: 5,
                //       strokeColor: '#775DD0'
                //     }
                //   ]
                // },
                // {
                //   x: '2017',
                //   y: [7332,8800],
                //   goals: [
                //     {
                //       name: 'Expected',
                //       value: 8700,
                //       strokeWidth: 5,
                //       strokeColor: '#775DD0'
                //     }
                //   ]
                // },
                // {
                //   x: '2018',
                //   y: [6553, 7500],
                //   goals: [
                //     {
                //       name: 'Expected',
                //       value: 7300,
                //       strokeWidth: 5,
                //       strokeColor: '#775DD0'
                //     }
                //   ]
                // }
            //   ]
            }
          ],
        options: {
          chart: {
            type: 'rangeBar',
            height: 350
          },
          plotOptions: {
            bar: {
              horizontal: false
            }
          },
          xaxis: {
            categories:  respTime?respTime.map(item =>item.CREW):[]
          },
          dataLabels: {
            enabled: true
          }
        },
    }
    return (
        <div id="chart">
<ReactApexChart options={state.options} series={state.series} type="rangeBar" height={350} />
</div>
    )
}

  