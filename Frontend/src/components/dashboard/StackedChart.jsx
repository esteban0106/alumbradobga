import React from 'react'
import ReactApexChart from 'react-apexcharts';

export const StackedChart = () => {
   const state = {
      
        series: [{
          name: 'PRODUCT A',
          data: [44, 55, 41, 67, 22, 43],
          goals: [
            {
              name: 'Mínimo',
              value: 45,
              strokeWidth: 5,
              strokeColor: '#775DD0'
            }
            ,
            {
              name: 'Maximo',
              value: 50,
              strokeWidth: 5,
              strokeColor: '#FEB019'
            },
            {
                name: 'Mínimo',
                value: 45,
                strokeWidth: 5,
                strokeColor: '#775DD0'
              }
              ,
              {
                name: 'Maximo',
                value: 50,
                strokeWidth: 5,
                strokeColor: '#FEB019'
              },
              {
                name: 'Mínimo',
                value: 45,
                strokeWidth: 5,
                strokeColor: '#775DD0'
              }
              ,
              {
                name: 'Maximo',
                value: 50,
                strokeWidth: 5,
                strokeColor: '#FEB019'
              }
          ]
        }, {
          name: 'PRODUCT B',
          data: [13, 23, 20, 8, 13, 27]
        }, {
          name: 'PRODUCT C',
          data: [11, 17, 15, 15, 21, 14]
        }, {
          name: 'PRODUCT D',
          data: [21, 7, 25, 13, 22, 8]
        }],
        options: {
          chart: {
            type: 'bar',
            height: 350,
            stacked: true,
            toolbar: {
              show: true
            },
            zoom: {
              enabled: true
            }
          },
          colors:['#fff','#f2522e', '#f2522e'],
          responsive: [{
            breakpoint: 480,
            options: {
              legend: {
                position: 'bottom',
                offsetX: -10,
                offsetY: 0
              }
            }
          }],
          plotOptions: {
            bar: {
              horizontal: false,
              borderRadius: 10
            },
          },
          xaxis: {
            type: 'datetime',
            categories: ['01/01/2011 GMT', '01/02/2011 GMT', '01/03/2011 GMT', '01/04/2011 GMT',
              '01/05/2011 GMT', '01/06/2011 GMT'
            ],
          },
          legend: {
            position: 'right',
            offsetY: 40
          },
          fill: {
            opacity: 1
          }
        },
      
      
      };
    return (
        <div id="chart">
<ReactApexChart options={state.options} series={state.series} type="bar" height={350} />
</div>
    )
}

  

