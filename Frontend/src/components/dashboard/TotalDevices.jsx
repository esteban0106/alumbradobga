import { Box, LinearProgress } from '@material-ui/core';
import { withStyles } from '@material-ui/styles';
import React from 'react'
import { useSelector } from 'react-redux'

export const TotalDevices = () => {
  const {devicesComuna} = useSelector(state => state.dashboard)
let totales = 0;

let dañados=0;
let activos=0;
devicesComuna.forEach( item =>{
  totales +=item.TOTAL;
  dañados+= parseInt( item.CONFALLAS);
  activos +=parseInt( item.ACTIVO)

})

const BorderLinearProgress = withStyles((theme) => ({
  root: {
    height: 10,
    borderRadius: 5,
  },
  colorPrimary: {
    backgroundColor: '#d5d5d5',
  },
  bar: {
    borderRadius: 5,
    backgroundColor: '#1a90ff',
  },
}))(LinearProgress);
  return (
    <div className="dashboard__values">
     <span>  <h4 >{totales}</h4> Dispositivos totales</span>
     <Box display="flex" alignItems="center">
      <Box width="100%" mr={1}>
      <BorderLinearProgress variant="determinate" value={totales?(totales/totales)*100:0} />

      </Box>
      <Box minWidth={35}>
        <span>{`${totales?(totales/totales)*100:0}%`}</span>
      </Box>
    </Box>
     <span>  <h4 >{dañados}</h4>Dispositivos con fallos</span>
     <Box display="flex" alignItems="center">
      <Box width="100%" mr={1}>
     <BorderLinearProgress variant="determinate" value={dañados?(parseInt( dañados)/totales)*100:0} />

      </Box>
      <Box minWidth={35}>
        <span>{`${dañados?(parseInt( dañados)/totales)*100:0}%`}</span>
      </Box>
    </Box>


     <span><h4 >{activos}</h4> Dispositivos activos</span>
     <Box display="flex" alignItems="center">
      <Box width="100%" mr={1}>
     <BorderLinearProgress variant="determinate" value={activos?(parseInt(activos)/totales)*100:0} />

      </Box>
      <Box minWidth={35}>
        <span>{`${activos?(parseInt( activos)/totales)*100:0}%`}</span>
      </Box>
    </Box>



      
    </div>
  )
}
