import moment from 'moment';
import React from 'react'
import ReactApexChart from 'react-apexcharts'
import { useSelector } from 'react-redux';







export const AreaChart = () => {
    const { last24, devicesList, idDevice } = useSelector(state => state.devices)
    let deviceInfo;
    devicesList?.forEach(item => {
        if (`${item['id-device']}` === `${idDevice}`) deviceInfo = item

    })
    let currentSeries = [];
    for (let i = 0; i < last24.length; i++) {
        deviceInfo &&

        currentSeries.push(deviceInfo['normal-current'])

    }



    const state = {

        series: [{
            name: "Corriente",
            data: last24.map(item => item['current-output'])
        },
        {
            name: "Corriente promedio",
            data: currentSeries
        }
        ],
        options: {

            chart: {
                id: 'sparkline1',
                type: ['area', 'line'],
                height: 160,
                sparkline: {
                    enabled: true
                },
                zoom: {
                    enabled: false
                }
            },
            dataLabels: {
                enabled: false
            },
            stroke: {
                curve: 'straight'
            },



            labels: last24.map(item => moment(item['fecha-output']).format('lll')),
            xaxis: {
                type: 'category',
            },
            yaxis: {
                opposite: true,
                labels: {
                    minWidth: 1,
                    formatter: function (value) {
                        return value + ' mA';
                      }
                }
            },
            legend: {
                show: false,
                horizontalAlign: 'left'
            },
            grid: {
                show: false,
            },
            fill: {
                opacity: 1,
            },

        },


    };


    return (

        <div id="chart" hidden={last24?.length === 0}>


            <ReactApexChart options={state.options} series={state.series} type="area" height={160} />
        </div>
    )
}

export const AreaChart2 = () => {
    const {  last24, devicesList, idDevice } = useSelector(state => state.devices);
    let deviceInfo;
    devicesList?.forEach(item => {
        if (`${item['id-device']}` === `${idDevice}`) deviceInfo = item

    })
    let voltageSeries = [];
    for (let i = 0; i < last24.length; i++) {
        deviceInfo &&

        voltageSeries.push(deviceInfo['normal-shunt-voltage'])

    }

    const state2 = {

        series: [{
            name: "Voltaje",
            data: last24.map(item => item['shunt-voltage-output'])
        },
        {
            name: "Voltaje promedio",
            data: voltageSeries
        }],
        options: {

            chart: {
                id: 'sparkline2',
                type: 'area',
                height: 160,
                sparkline: {
                    enabled: true
                },
                zoom: {
                    enabled: false
                }
            },
            dataLabels: {
                enabled: false
            },
            stroke: {
                curve: 'straight'
            },



            labels: last24.map(item => moment(item['fecha-output']).format('lll')),

            xaxis: {
                type: 'category',
            },
            yaxis: {
                opposite: true,
                labels: {
                    minWidth: 1,
                    formatter: function (value) {
                        return `${value} V`;
                      }
                }
            },
            legend: {
                show: false,
                horizontalAlign: 'left'
            },
            grid: {
                show: false,
            },
            fill: {
                opacity: 1,
            },

        },


    };
    return (

        <div id="chart" hidden={last24?.length === 0}>
            <ReactApexChart options={state2.options} series={state2.series} type="area" height={160} />

        </div>
    )
}

export const AreaChart3 = () => {
    const {  last24, devicesList, idDevice } = useSelector(state => state.devices);
    let deviceInfo;
    devicesList?.forEach(item => {
        if (`${item['id-device']}` === `${idDevice}`) deviceInfo = item

    })
    let powerSeries = [];
    for (let i = 0; i < last24.length; i++) {
        deviceInfo &&
        powerSeries.push(deviceInfo['normal-power'])

    }
    const state3 = {

        series: [{
            name: "Potencia",
            data: last24.map(item => item['power-output'])
        },
        {
            name: "Potencia promedio",
            data: powerSeries
        }],
        options: {

            chart: {
                id: 'sparkline3',
                type: 'area',
                height: 160,
                sparkline: {
                    enabled: true
                },
                zoom: {
                    enabled: false
                }
            },
            dataLabels: {
                enabled: false
            },
            stroke: {
                curve: 'straight'
            },



            labels: last24.map(item => moment(item['fecha-output']).format('lll')),

            xaxis: {
                type: 'category',
            },
            yaxis: {
                opposite: true,
                labels: {
                    minWidth: 1,
                    formatter: function (value) {
                        return value + ' W';
                      }
                }
            },
            legend: {
                show: false,
                horizontalAlign: 'left'
            },
            grid: {
                show: false,
            },
            fill: {
                opacity: 1,
            },

        },


    };

    return (

        <div id="chart" hidden={last24?.length === 0}>
            <ReactApexChart options={state3.options} series={state3.series} type="area" height={160} />

        </div>
    )
}





