import { FormControl, Grid, InputLabel, MenuItem, Select,Autocomplete } from '@material-ui/core';
import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { useForm } from '../../hooks/useForm';
import { CssTextField } from '../ui/InputOutlined';
import validator from 'validator'
import { startAddDevices, startGetCrew } from '../../actions/devicesActions';
import { startLoadingCommunas } from '../../actions/globalActions';


export const CreateDevices = ({close}) => {

    const dispatch = useDispatch();
    useEffect(() => {

        dispatch(startLoadingCommunas());

    }, [dispatch]);

    

    const { communeList } = useSelector(state => state.global);
    const { crews } = useSelector(state => state.devices);

    const [valid, setValidator] = useState({
        email: '',
        lat: '',
        lon: '',
        desc: '',
        shunt: '',
        power: '',
        current: '',
        working: '',
        address: '',
        comuna: '',
        crew:'',
    })
    // const dispatch = useDispatch();

    const [formValues, handleInputChange] = useForm({
        nombre: "",
        lat: "",
        lon: "",
        desc: "",
        shunt: "",
        power: "",
        current: "",
        working: "",
        address: "",
        comuna: "",
        crew:"",

    })

    const {
        nombre,
        lat,
        lon,
        desc,
        shunt,
        power,
        current,
        working,
        address,
        comuna,
        crew
    } = formValues;

    useEffect(() => {

        dispatch(startGetCrew(comuna));

    }, [dispatch, comuna]);
    const handleValidator = (e, type) => {
        handleInputChange(e);
        const value = e.target.value;
        switch (type) {
            case "text":
                if (value.length < 1) {
                    setValidator({
                        ...valid,
                        [e.target.name]: '* El campo es obligatorio'
                    })
                } else {
                    setValidator({
                        ...valid,
                        [e.target.name]: null
                    })
                }
                break;
            case "email":
                if (!validator.isEmail(value)) {
                    setValidator({
                        ...valid,
                        [e.target.name]: '* El email no es valido'
                    })

                } else {
                    setValidator({
                        ...valid,
                        [e.target.name]: null
                    })
                }
                break;
            default:
                if (value.length === 0) {

                    setValidator({
                        ...valid,
                        [e.target.name]: '* Campo obligatorio'

                    })
                } else {
                    setValidator({
                        ...valid,
                        [e.target.name]: null

                    })
                }
                break;
        }
    }

    const handleSubmit = (e) => {


        e.preventDefault();
        if (isFormValid()) {

            dispatch(startAddDevices({


                "name-device": nombre,
                "lat-device": lat,
                "lon-device": lon,
                "description-luminary": desc,
                "normal-shunt-voltage": shunt,
                "normal-current": current,
                "normal-power": power,
                "working": working,
"crew": crew,
                "address": { "name-address": address, "id-commune": comuna }
            }));

            close()
        }
    }

    const isFormValid = () => {


        if (!valid.email && !valid.lon && nombre && lat && crew) {
            return true
        } else {
            return false
        }

    }
    return (
        <form key="formCreateDevices" onSubmit={handleSubmit}>

            <Grid container spacing={2}
                alignItems="center" >

                <Grid item sm={6} xl={4} md={4} xs={12} >
                    <CssTextField

                        name="nombre"
                        value={nombre}
                        onChange={(e) => handleValidator(e, 'text')}
                        placeholder="INA219-1"
                        type="text"
                        label="Nombre"
                        variant='outlined'
                        error={!!valid.nombre}
                        helperText={(valid.nombre ? valid.nombre : '')}

                        size="small"
                        autoComplete='off'

                    />
                </Grid>
                <Grid item sm={6} xl={4} md={4} xs={12} >


                    <CssTextField
                        name="lat"
                        value={lat}
                        onChange={(e) => handleValidator(e, 'double')}
                        placeholder="0.000000"


                        type="text"
                        label="Latitud"
                        variant='outlined'
                        error={!!valid.lat}
                        helperText={(valid.lat ? valid.lat : '')}
                        size="small"
                        autoComplete='off'

                    />

                </Grid>

                <Grid item sm={6} xl={4} md={4} xs={12}>


                    <CssTextField
                        name="lon"
                        value={lon}
                        onChange={(e) => handleValidator(e, 'double')}
                        placeholder="0.000000"


                        type="text"
                        label="Longitud"
                        variant='outlined'
                        error={!!valid.lon}
                        helperText={(valid.lon ? valid.lon : '')}
                        size="small"
                        autoComplete='off'

                    />
                </Grid>

                <Grid item sm={6} xl={4} md={4} xs={12} >


                    <CssTextField
                        name="desc"
                        value={desc}
                        placeholder="Descripción de la luminaría"
                        onChange={(e) => handleValidator(e, 'text')}


                        type="text"
                        label="Descripción"
                        variant='outlined'
                        error={!!valid.desc}
                        helperText={(valid.desc ? valid.desc : '')}
                        size="small"
                        autoComplete='off'

                    />
                </Grid>

                <Grid item sm={6} xl={4} md={4} xs={12} >


                    <CssTextField
                        name="shunt"
                        value={shunt}
                        onChange={(e) => handleValidator(e, 'double')}
                        placeholder='1.4'
                        type="text"
                        label="Voltaje normal shunt"
                        variant='outlined'
                        error={!!valid.shunt}
                        helperText={(valid.shunt ? valid.shunt : '')}
                        size="small"
                        autoComplete='off'

                    />
                </Grid>

                <Grid item sm={6} xl={4} md={4} xs={12} >


                    <CssTextField
                        name="power"
                        value={power}
                        onChange={(e) => handleValidator(e, 'double')}
                        placeholder="0.5"

                        type="text"
                        label="Potencia normal"
                        variant='outlined'
                        error={!!valid.power}
                        helperText={(valid.power ? valid.power : '')}
                        size="small"
                        autoComplete='off'

                    />
                </Grid>

                <Grid item sm={6} xl={4} md={4} xs={12} >


                    <CssTextField
                        name="current"
                        value={current}
                        onChange={(e) => handleValidator(e, 'double')}

                        placeholder="0.5"

                        type="text"
                        label="Corriente normal"
                        variant='outlined'
                        error={!!valid.current}
                        helperText={(valid.current ? valid.current : '')}
                        size="small"
                        autoComplete='off'

                    />
                </Grid>

                <Grid item sm={6} xl={4} md={4} xs={12} >
                    <FormControl fullWidth={true} size="small" variant='outlined'>
                        <InputLabel id="demo-simple-select-label">¿Está funcionando?</InputLabel>
                        <Select
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            name="working"
                            value={working}
                            onChange={(e) => handleValidator(e, 'text')}
                            error={!!valid.working}
                            // helperText={(valid.working ? valid.working : '')}
                            size="small"
                            autoComplete='off'
                            label="¿Está funcionando?"



                        >
                            <MenuItem value={true}>Si</MenuItem>
                            <MenuItem value={false}>No</MenuItem>
                        </Select>
                    </ FormControl>
                    {/* <CssTextField
                        name="working"
                        value={working}
                        onChange={(e) => handleValidator(e, 'text')}


                        type="text"
                        label="¿Está Funcionando?"
                        variant='outlined'
                        error={!!valid.working}
                        helperText={(valid.working ? valid.working : '')}
                        size="small"
                        autoComplete='off'

                    /> */}
                </Grid>



                <Grid item sm={6} xl={4} md={4} xs={12} >


                    <CssTextField
                        name="address"
                        value={address}
                        onChange={(e) => handleValidator(e, 'text')}


                        type="text"
                        label="Dirección de ubicación"
                        variant='outlined'
                        error={!!valid.address}
                        helperText={(valid.address ? valid.address : '')}
                        size="small"
                        autoComplete='off'

                    />
                </Grid>
                <Grid item sm={6} xl={4} md={4} xs={12} >

                    <Autocomplete
                        id="combo-box-demo"
                        loading={communeList.length === 0}
                        autoSelect={true}
                        options={communeList}
                        getOptionLabel={(option) => option['name-commune']}
                        style={{ width: 300 }}
                        isOptionEqualToValue={(option, value) => option['id-commune'] === value['id-commune']}
                        // getoptionselected={(option, value) => option['id-commune'] === value['id-commune']}
                        onChange={(event, value) =>
                            handleValidator({
                                target: {
                                    value: value['id-commune'],
                                    name: 'comuna'
                                }
                            },
                                'text')}


                        renderInput={(params) => <CssTextField {...params}
                            name="comuna"
                            value={comuna}


                            type="text"
                            label="Comuna"
                            variant='outlined'
                            error={!!valid.comuna}
                            helperText={(valid.comuna ? valid.comuna : '')}
                            size="small"
                            autoComplete='off'

                        />}
                    />

                </Grid>
                <Grid item sm={6} xl={4} md={4} xs={12} >

                    <Autocomplete
                        id="combo-box-demo"
                        loading={crews.length === 0}
                        autoSelect={true}
                        options={crews}
                        getOptionLabel={(option) => `Cuadrilla ${option['id-crew']}`}
                        style={{ width: 300 }}
                        isOptionEqualToValue={(option, value) => option['id-crew'] === value['id-crew']}

                        // getoptionselected=
                        onChange={(event, value) =>
                            handleValidator({
                                target: {
                                    value: value['id-crew'],
                                    name: 'crew'
                                }
                            },
                                'text')}


                        renderInput={(params) => <CssTextField {...params}
                            name="crew"
                            value={crew}


                            type="text"
                            label="Cuadrilla"
                            variant='outlined'
                            error={!!valid.crew}
                            helperText={(valid.crew ? valid.crew : '')}
                            size="small"
                            autoComplete='off'

                        />}
                    />

                </Grid>

            </Grid>
            <Grid item container
                direction="row"
                justify="center"
                alignItems="center">


                <button className="auth__button" type="submit" disabled={!isFormValid()} >
                    Ingresar
            </button>


            </Grid>

        </form>
    )
}
