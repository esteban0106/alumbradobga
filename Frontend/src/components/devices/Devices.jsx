import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import {  startGetLast24 } from '../../actions/devicesActions';
import { Map } from '../map/Map';
import AddIcon from '@material-ui/icons/Add';
import Modal from 'react-modal';
import { CreateDevices } from './CreateDevices';
import {  IconButton, Paper } from '@material-ui/core';
import CancelIcon from '@material-ui/icons/Cancel';
import AddBoxIcon from '@material-ui/icons/AddBox';
import { DevicesTable } from './DevicesTable';
import { ValuesCard } from './ValuesCard';
import { FiltroDevices } from './FiltroDevices';
import moment from 'moment';

export const Devices = () => {



  const dispatch = useDispatch();
  const { devicesList, idDevice } = useSelector(state => state.devices);
  const { user } = useSelector(state => state.auth);




useEffect(() => {
  if(idDevice && ![4,3].includes( user['id-rol']) )
  dispatch(startGetLast24(idDevice))
  
}, [dispatch,idDevice])


  const [modalIsOpen, setIsOpen] = useState(false);
  function openModal() {
    setIsOpen(true);
  }



  function closeModal() {
    setIsOpen(false);
  }

  return (
    <div className="devices__container">
      <Paper >
      <div className="devices__header">

        <FiltroDevices />
        <h3>{idDevice && `Dispositivo ${idDevice}`}</h3>
        {! [4,3].includes( user['id-rol'])  &&
          <button onClick={openModal} className="btn devices__button-crear"> <AddIcon /><span> Añadir dispositivo</span></button>
        }
      </div>
        </Paper> 
      <Modal
        isOpen={modalIsOpen}
        // onAfterOpen={afterOpenModal}
        onRequestClose={closeModal}
        className="modal__main"
        overlayClassName="modal__overlay"
        contentLabel="Example Modal"
        closeTimeoutMS={200}
        ariaHideApp={false}

      >

        <div className="modal__title">

          <span className="modal__icon-title">
            <AddBoxIcon />
              Añadir dispositivos</span>
          <IconButton color="primary" aria-label="upload picture" onClick={closeModal} component="span">
            <CancelIcon />
          </IconButton></div>


        <CreateDevices close={()=>closeModal()}/>

      </Modal>
     { (user['id-rol'] !== 3)&&
      <ValuesCard />
}

      <div className="map__container">
        < Map  />

      </div>


      <DevicesTable rows={devicesList} columns={[
        {
          id: "id-device",
          align: "center",
          minWidth: "100px",
          label: "ID"
        },
        {
          id: "name-device", align: "center",
          minWidth: "100px",
          label: "Nombre"
        },
        {
          id: "commune", align: "center",
          minWidth: "100px",
          label: "Comuna"
        },
        {
          id: "address", align: "center",
          minWidth: "200px",
          label: "Dirección"
        },
        {
          id: "lon-device", align: "center",
          minWidth: "100px",
          label: "Longitud"
        },
        {
          id: "description-luminary", align: "center",
          minWidth: "100px",
          label: "Descripción"
        },
        {
          id: "normal-shunt-voltage", align: "center",
          minWidth: "100px",
          label: "Voltaje shunt"
        },
        {
          id: "normal-current", align: "center",
          minWidth: "100px",
          label: "Corriente"
        },
        {
          id: "normal-power", align: "center",
          minWidth: "100px",
          label: "Potencia"
        },
        {
          id: "working", align: "center",
          minWidth: "100px",
          label: "Presenta fallos",
          format: (value) => (value===1) ?'No':'Si' ,
        },
        {
          id: "created-at", align: "center",
          minWidth: "100px",
          label: "Fecha",
          format: (value) => moment.utc( value ).format('lll').toLocaleString('es-ES'),

        }
      ]} />
      
    </div>
  )
}

// "id-device": 2,
//         "name-device": "luminaria prueba update",
//         "lat-device": 7.105955414287668,
//         "lon-device": -73.1142080730143,
//         "description-luminary": "LUMINARIA ANTIGUA",
//         "normal-shunt-voltage": 0.95,
//         "normal-current": 2,
//         "normal-power": 0.15,
//         "working": true,
//         "created-at": "2021-03-03T14:54:38.000Z",
//         "created-by": "",
//         "updated-at": "2021-03-05T12:56:38.000Z",
//         "updated-by": "",
//         "address": "Puerta del sol",
//         "commune": "LA CONCORDIA",
//         "city": "BUCARAMANGA",
//         "department": "SANTANDER"


//  "name-device": "LUM-123",
// "lat-device": 7.105955414287668,
// "lon-device": -73.1142080730143,
// "description-luminary": "LUMINARIA PARQUE",
// "normal-shunt-voltage": 0.95,
// "normal-current": 2,
// "normal-power": 0.15,
// "working":true,
// "created-by":"mafevera",
// "updated-by":"mafevera",
// "address":{ "name-address": "Provenza", "id-commune": 18 }