import {  Paper, Table, TableBody, TableCell, TableContainer, TableHead, TablePagination, TableRow } from '@material-ui/core';
import { createTheme, ThemeProvider } from '@material-ui/core/styles';

import React, { useState } from 'react'
import { esES } from '@material-ui/core/locale';

export const DevicesTable = ({rows, columns}) => {

    const [page, setPage] = useState(0);
    const [rowsPerPage, setRowsPerPage] = useState(5);
  
    const handleChangePage = (event, newPage) => {
      setPage(newPage);
    };

    const theme = createTheme(
      {
    
      },
      esES,
    );
    // esES
    const handleChangeRowsPerPage = (event) => {
      setRowsPerPage(+event.target.value);
      setPage(0);
    };
    return (
        <div className="table__main">
        <Paper >
        <ThemeProvider theme={theme}>

        <TableContainer >
          <Table stickyHeader aria-label="sticky table">
            <TableHead>
              <TableRow>
                {columns.map((column) => (
                  <TableCell
                    key={column.id}
                    align={column.align}
                    style={{ minWidth: column.minWidth }}
                  >
                    {column.label}
                  </TableCell>
                ))}
              </TableRow>
            </TableHead>
            <TableBody>
              {rows?.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((row) =>{
                return (
                  <TableRow hover role="checkbox" tabIndex={-1} key={row['id-device']}>
                    {columns.map((column) => {
                      const value = row[column.id];
                      return (
                        <TableCell key={column.id} align={column.align}>
                          {
                          column.format? column.format(value) : value
                          
                          }
                        </TableCell>
                      );
                    })}
                  </TableRow>
                );
              })}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
        
          rowsPerPageOptions={[5, 10, 25]}
          component="div"
          count={rows?rows.length:0}
          rowsPerPage={rowsPerPage}
          page={page}
          onRowsPerPageChange={handleChangeRowsPerPage}

          // onChangeRowsPerPage={handleChangeRowsPerPage}
          onPageChange={handleChangePage}
        />
     </ThemeProvider>
      </Paper>
      </div>
    )
}
