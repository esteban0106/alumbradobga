import { Autocomplete, IconButton } from '@material-ui/core'
import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { resetDevices, startGetDevices } from '../../actions/devicesActions';
import { startLoadingCommunas } from '../../actions/globalActions';
import { useForm } from '../../hooks/useForm';
import { CssTextField } from '../ui/InputOutlined';
import SearchIcon from '@material-ui/icons/Search';

export const FiltroDevices = () => {
    const dispatch = useDispatch();
    const [value, handleForm] = useForm({
        comuna: null,
        idDev: ''

    })
    const {comuna, idDev} = value
    useEffect(() => {

        dispatch(resetDevices())

        dispatch(startGetDevices(comuna, null));

    }, [dispatch, comuna])

  const handleClick = (e) =>{
e.preventDefault();
if(idDev.length > 0){
    dispatch(resetDevices())

    dispatch(startGetDevices(null, idDev));
}
  }

    useEffect(() => {
        dispatch(startLoadingCommunas())


    }, [dispatch]);

    const { communeList } = useSelector(state => state.global);

    return (
        <div className="device__filtros">
            <Autocomplete
                id="combo-box-demo"
                loading={communeList.length === 0}
                autoSelect={true}
                options={communeList}
                getOptionLabel={(option) => option['name-commune']}
                style={{ width: "200px" }}
                isOptionEqualToValue={(option, value) => option['id-commune'] === value['id-commune']}
                onChange={(event, value) =>
                    handleForm({
                        target: {
                            value: value ? value['id-commune'] : null,
                            name: 'comuna'
                        }
                    },
                    )}

                // getoptionselected=


                renderInput={(params) => <CssTextField {...params}
                    name="crew"
                    value={comuna}


                    type="text"
                    label="Comuna"
                    variant='outlined'

                    size="small"
                    autoComplete='off'

                />}
            />

            <CssTextField

                name="idDev"
                value={idDev}
                onChange={handleForm}
                placeholder="1"
                type="number"
                label="Nombre"
                variant='outlined'
                style={{width:'160px'}}

                size="small"
                autoComplete='off'

            />

<IconButton type="submit" onClick={handleClick} aria-label="search">
        <SearchIcon />
      </IconButton>

        </div>
    )
}
