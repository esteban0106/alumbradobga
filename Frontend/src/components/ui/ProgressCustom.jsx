import { Backdrop, CircularProgress } from '@material-ui/core'
import { makeStyles } from '@material-ui/styles';

import React from 'react'
import { shallowEqual, useSelector } from 'react-redux';

export const ProgressCustom = () => {
    const { loading } = useSelector(state => state.ui,shallowEqual)
    const useStyles = makeStyles((theme) => ({
        backdrop: {
            zIndex: 1000,
            color: '#fff',
        },
    }));
    const classes = useStyles();
    return (
        <Backdrop className={classes.backdrop} open={loading} >
        <CircularProgress  />
    </Backdrop>
    )
}
