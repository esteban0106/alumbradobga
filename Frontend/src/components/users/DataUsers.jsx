import { IconButton, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TablePagination, TableRow } from '@material-ui/core';
import { createTheme, ThemeProvider } from '@material-ui/core/styles';

import React, { useEffect, useState } from 'react'
import { esES } from '@material-ui/core/locale';
import { useDispatch, useSelector } from 'react-redux';
import { startGetUsers } from '../../actions/userActions';
import EditIcon from '@material-ui/icons/Edit';
import Swal from 'sweetalert2';
import { httpToken } from '../../utils/fetch';


export const DataUsers = () => {
  const { users } = useSelector(state => state.user)
  const dispatch = useDispatch()
  useEffect(() => {
    dispatch(startGetUsers())

  }, [dispatch])

  const columns = [
    {
      id: "edit", align: "center",
      minWidth: "30px",
      label: ""
    },
    {
      id: "nombre", align: "center",
      minWidth: "200px",
      label: "Nombre"
    },

    {
      id: "apellido", align: "center",
      minWidth: "200px",
      label: "Apellidos"
    },
    {
      id: "email", align: "center",
      minWidth: "150px",
      label: "Email"
    },
    {
      id: "rol", align: "center",
      minWidth: "150px",
      label: "Rol"
    },
    {
      id: "crew", align: "center",
      minWidth: "150px",
      label: "Cuadrilla"
    },
    {
      id: "telefono", align: "center",
      minWidth: "150px",
      label: "Telefono"
    },
    {
      id: "cargo", align: "center",
      minWidth: "150px",
      label: "Cargo"
    },
    {
      id: "fecha", align: "center",
      minWidth: "150px",
      label: "Fecha nacimiento"
    },


  ];

  const rows = users;


  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(5);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const theme = createTheme(
    {

    },
    esES,
  );
  // esES
  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  

  const [open, setopen] = useState(false)
  const handleClick = (email)=>{
    Swal.fire({
      text: 'Cambiar contraseña',
      input: 'password',
      inputAttributes: {
        autocapitalize: 'off'
      },
      showCancelButton: true,
      confirmButtonText: 'Guardar',
      showLoaderOnConfirm: true,
      
      preConfirm: (password) => {
        try{
        if(password.length > 5)
        return httpToken(`auth/change`,{"username":email, "password":password}, 'POST')
          .then(response => {
            if (!response.ok) {
              throw new Error(response.statusText)
            }
            return response.json()
          })
          .catch(error => {
            Swal.showValidationMessage(
              `Request failed: ${error}`
            )
          })
          else throw new Error('La contraseña debe tener 6 caracteres o más')
        }catch(e){
          Swal.showValidationMessage(
            ` ${e}`
          )
        }
      },
      allowOutsideClick: () => !Swal.isLoading()
    }).then((result) => {
      
      if (result.isConfirmed) {
        Swal.fire(
          "",
         `Contraseña guardada`,
          'success'
          
        )

      }
    })
  }
  return (
    <div className="user__table">
      <Paper >
        <ThemeProvider theme={theme}>

          <TableContainer >
            <Table stickyHeader aria-label="sticky table">
              <TableHead >
                <TableRow >
                  {columns.map((column) => (
                    <TableCell
                      key={column.id}
                      align={column.align}
                      style={{ minWidth: column.minWidth, fontWeight: 'bold' }}

                    >
                      {column.label}
                    </TableCell>
                  ))}
                </TableRow>
              </TableHead>
              <TableBody>
                {rows?.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((row) => {
                  return (
                    <TableRow hover role="checkbox" tabIndex={-1} key={row['email']}>
                     
                      {columns.map((column) => {
                        const value = row[column.id];

                        return (
                        
                          <TableCell key={column.id} align={column.align}>
                            { column.id==="edit"
                            ?<IconButton color="primary" aria-label="upload picture" component="span" onClick={()=>handleClick(row['email'])}>
                            <EditIcon />
                          </IconButton>
                          :
                              column.format ? column.format(value) : value

                            }
                          </TableCell>
                        );
                      })}
                    </TableRow>
                  );
                })}
              </TableBody>
            </Table>
          </TableContainer>
          <TablePagination

            rowsPerPageOptions={[5, 10, 25]}
            component="div"
            count={rows ? rows.length : 0}
            rowsPerPage={rowsPerPage}
            page={page}
            onRowsPerPageChange={handleChangeRowsPerPage}

            // onChangeRowsPerPage={handleChangeRowsPerPage}
            onPageChange={handleChangePage}
          />
        </ThemeProvider>
      </Paper>
    </div>
  )
}
