import { crewsTypes } from "../types/crewTypes";

const initialState = {
    crews: [],
    infoCrew: [],
   idcrew:null,

}

export const crewsReducer = (state = initialState, action) => {
    switch (action.type) {
        case crewsTypes.loadCrews:

            return {
                ...state,
                crews: action.payload
            }

        case crewsTypes.loadFailsAndEmployees:

            return {
                ...state,
                infoCrew: action.payload
            }
            case crewsTypes.loadIdCrew:

                return {
                    ...state,
                    idcrew: action.payload
                }
        
    


        default:
            return state;
    }
}