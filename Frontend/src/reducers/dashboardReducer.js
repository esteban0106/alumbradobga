import { dashboardTypes } from "../types/dashboardTypes";

const initialState = {
    failsComuna: [],
    CVPTotal: [],
    topDevices: [],
    respTime: [],
    errorTypes: [],
    devicesComuna: [],
    outputType :[]

}

export const dashboardReducer = (state = initialState, action) => {
    switch (action.type) {
        case dashboardTypes.loadFailsComuna:

            return {
                ...state,
                failsComuna: action.payload
            }

        case dashboardTypes.loadCVPtotal:

            return {
                ...state,
                CVPTotal: action.payload
            }
        case dashboardTypes.loadTopDevices:

            return {
                ...state,
                topDevices: action.payload
            }
        case dashboardTypes.loadRespTime:

            return {
                ...state,
                respTime: action.payload
            }
        case dashboardTypes.loadTypeError:

            return {
                ...state,
                errorTypes: action.payload
            }

            case dashboardTypes.loadDevicesComuna:

            return {
                ...state,
                devicesComuna: action.payload
            }
            case dashboardTypes.outputxtype:

                return {
                    ...state,
                    outputType: action.payload
                }
            
        default:
            return state;
    }
}