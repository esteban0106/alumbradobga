import { devicesTypes } from "../types/devicesTypes";

const initialState = {
    devicesList: [],
    last24: [],
    client: null,
    idDevice: null,
    crews: [],
    values: {
        null:
            { current: 'Off', voltage: 'Off', power: 'Off', }
    }


}

export const devicesReducer = (state = initialState, action) => {
    switch (action.type) {
        case devicesTypes.devicesLoad:

            return {
                ...state,
                devicesList: action.payload
            }

        case devicesTypes.addDevices:
            return {
                ...state,
                devicesList: [
                    ...state.devicesList,
                    action.payload]
            }
        case devicesTypes.selectIdDevice:
            return {
                ...state,
                idDevice: action.payload
            }

        case devicesTypes.addClient:
            return {
                ...state,
                client: action.payload
            }
        case devicesTypes.addValuesDevices:

            return {
                ...state,
                values: {
                    ...state.values,
                    [action.payload.id]: action.payload
                },
            }

        case devicesTypes.addLast24:

            return {
                ...state,
                last24: action.payload

            }
        case devicesTypes.loadCrew:

            return {
                ...state,
                crews: action.payload

            }

        case devicesTypes.resetDevices:
            return initialState;

        default:
            return state;
    }
}