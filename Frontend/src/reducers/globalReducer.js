import { globalTypes } from "../types/globalTypes";

const initialState = {
    communeList: [],
    departmentList: [],
    cityList: [],
}

export const globalReducer = (state = initialState, action) => {
    switch (action.type) {
        case globalTypes.globalLoadCommune:

            return {
                ...state,
                communeList: action.payload 
            }
            
       
        default:
            return state;
    }
}