import { globalType } from "../types/uiTypes";

const initialState = {
    hide: false,
    loading: false
}

export const uiReducer = (state = initialState, action) => {
    switch (action.type) {
        case globalType.uiHideSidebar:

            return {
                ...state,
                hide: true
            }
        case globalType.uiShowSidebar:

            return {
                ...state,
                hide: false
            }
        case globalType.uiStartLoading:

            return {
                ...state,
                loading: true
            }
        case globalType.uiFinishLoading:

            return {
                ...state,
                loading: false
            }

        default:
            return state;
    }
}