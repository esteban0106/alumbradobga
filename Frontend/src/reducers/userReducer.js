import { userType } from "../types/userTypes";

const initialState = {
    employeeCreated: false,
    userCreated: false,
    users:[]
}

export const userReducer = (state = initialState, action) => {
    switch (action.type) {
        case userType.crearUsuario:

            return {
                ...state,
                userCreated: action.payload
            }
        case userType.crearEmpleado:

            return {
                ...state,
                employeeCreated: action.payload
            }

            case userType.loadUsers:

                return {
                    ...state,
                    users: action.payload
                }
        case userType.resetUser:

            return initialState
            
     
        default:
            return state;
    }
}