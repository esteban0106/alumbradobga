

import React, { useEffect, useRef, useState } from "react";
import {
    BrowserRouter as Router,
    Switch,
    Redirect,
} from "react-router-dom";
import { PublicRoute } from "./PublicRoute";
import { PrivateRoute } from "./PrivateRoute";
import { DashBoardRouter } from "./DashboardRouter";
import { AuthRouter } from "./AuthRouter";
import {  useDispatch, useSelector } from "react-redux";
import { startChecking } from "../actions/authActions";
import { LeaderRoute } from "./LeaderRoute";
import { LoadingPage } from "../components/ui/LoadingPage";
import { ProgressCustom } from "../components/ui/ProgressCustom";
export const AppRouter = () => {
    const dispatch = useDispatch();

    const { checking, token, user } = useSelector(state => state.auth);
    let rol;

 

    if (user)
        rol = user['id-rol'];

    useEffect(() => {
        dispatch(startChecking());

    }, [dispatch])
    if (checking) {
        return (<LoadingPage />);
    }
    return (
        <Router>
            <div>
           
            <ProgressCustom />
                <Switch>

                    <PublicRoute path="/auth" isAuth={!!token} component={AuthRouter} />
                    {
                  
                        ([4,3].includes(rol) ) ?
                            <PrivateRoute

                                path="/"
                                isAuth={!!token}
                                component={LeaderRoute} />
                            :
                            <PrivateRoute

                                path="/"
                                isAuth={!!token}
                                component={DashBoardRouter} />
                    }
                    <Redirect to='/auth/login' />

                </Switch>
            </div>

        </Router>
    )
}
