import React from 'react'
import { Redirect, Route, Switch } from 'react-router'
import { LoginScreen } from '../components/auth/LoginScreen'

export const AuthRouter = () => {
    return (

        <div className="auth__main">

            <div className="auth__box-container">
                <div className="auth__box-container-decoration">

                </div>
                <Switch>
                    <Route
                        exact
                        path="/auth/login"
                        component={LoginScreen}
                    />


                    <Redirect to='/auth/login' />
                </Switch>
            </div>
            <aside className="auth__image">

            </aside>
        </div>
    )
}
