import React from 'react'
import {
    Redirect,
    Route,
  
} from 'react-router'
import PropTypes from 'prop-types'

export const PrivateRoute = ({
        isAuth,
        rol,
        component: Component,
        ...rest
    }) => {
        localStorage.setItem('lastPath', rest.location.pathname +  rest.location.search)

        return ( <
            Route {
                ...rest
            }
            component = {
                (props) => {
                    return (isAuth  ) ?
                        ( < Component {
                                ...props
                            }
                            />): ( < Redirect to = "/auth/login" / > )
                        }
                }
                />
            )
        }
        PrivateRoute.propTypes = {
            isAuth: PropTypes.bool.isRequired,
            component: PropTypes.func.isRequired
        }