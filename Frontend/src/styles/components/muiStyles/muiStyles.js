import { makeStyles } from '@material-ui/styles';


export const useStyles = makeStyles({
   
root:{
    fontFamily: 'Open Sans',
    margin: '0px',
    color: '#3c3c3c',
    width:'80%',

    '&:hover':{
        borderColor:'lighten("#4c3073",20) !important'
    }
},




});

