
export const devicesTypes = {
    devicesLoad: "[devices] load devices",
    addDevices:  "[devices] add new device",
    selectIdDevice: "[devices] select id device",
    addValuesDevices: "[devices] add values of devices",
    addClient: "[devices] add client",
    addLast24: "[devices] add last 24",
    loadCrew: "[devices] load crews",
    resetDevices: "[devices] reset devices",
    
}