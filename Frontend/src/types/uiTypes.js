
export const globalType = {
    uiShowSidebar: "[ui] Show sidebar",
    uiHideSidebar: "[ui] Hide sidebar",
    uiStartLoading: "[ui] Start loading async actions",
    uiFinishLoading: "[ui] Finish loading async actions",
    }