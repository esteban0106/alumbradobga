 const process_msg = async(topic, message) => {
        if (topic === "values") {
            let msg = message.toString();
            let sp = msg.split(",");
            let shunt = sp[0];
            let current = sp[1];
            let power = sp[2];
            let voltage = sp[3];
            let idDevice = sp[4];
    
            console.log(
                `shunt: ${shunt}, current: ${current}, Power: ${power}, Voltage: ${voltage}, Device: ${idDevice}`
            );

            return {id:idDevice,current, voltage, power}
        }
    };
   
    export{
        process_msg
    }